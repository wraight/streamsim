###
### make html summary files in directory matching strings
###
import glob
import argparse
import subprocess
import os
import datetime
import time


######################
### parsing
######################

def GetArgs():
    parser = argparse.ArgumentParser(description=__file__)

    parser.add_argument('--dir', help='directory for files')
    parser.add_argument('--suffix', help='output directories suffix')
    parser.add_argument('--snips', nargs='+', help='filter files')
    parser.add_argument('--name', help='index file name')

    args = parser.parse_args()

    print "args:",args

    argDict={'dir':"output", 'suffix':"NYS", 'snips':[], 'name':"NYS" }

    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict

######################
### main
######################

def main():

    argDict=GetArgs()
    print argDict

    my_files = glob.glob(argDict['dir']+'/*')
    print "files in directory:",len(my_files)

    for s in argDict['snips']:
        my_files=[ f for f in my_files if s in f]
        print "\tselecting ",s,":",len(my_files)

    if len(my_files)<1:
        print "no files found matching. exiting"
        return

    mdStr="md"
    if "NYS" not in argDict['suffix']:
        mdStr+=argDict['suffix']
    commandStr="python "+__file__+" "+" ".join(["--"+str(k)+" "+str(v) for k,v, in argDict.items()])
    markName=mdStr+"/index_of_"+argDict['dir'][argDict['dir'].rfind("/")+1::]+".md"

    contents=""
    # title
    contents+="# Index of: "+argDict['dir']+"\n\n---\n\n"
    # meta-info
    contents+="## meta-information \n\n"
    contents+="### date \n\n"+datetime.datetime.now().strftime("%Y-%m-%d %H:%M")+"\n\n"
    contents+="### command \n\n(pwd:"+os.getcwd()+")\n\n"
    contents+="```\n"+commandStr+"\n```\n\n---\n\n"
    # content
    contents+="### Files in directory: \n\n"
    contents+="\n".join(["\n["+f+"]("+os.getcwd()+"/"+f+")" for f in my_files])

    ### write markdown file
    newFile =open(markName, "w")
    newFile.write(contents)
    newFile.close()

    ### generate html from markdown
    htmlName=markName.replace("md","html")
    print subprocess.check_output(['multimarkdown', markName, '-o', htmlName])
    print "wrote:",htmlName




if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "Total scan time: ",(end-start),"seconds"
    print "### out",__file__,"###"


# Daterate info for _somePosition_

---

## meta-information

### date
_someDate_

### command

(pwd: _someDir_)


```
_someCommand_
```
---

## steam information (edited)

| stream occupancy || stream ingredient ||
| --- | :--- | --- | :--- |
_info_

## steam statistics (all)

| stream stat | value |
| --- | :--- |
_statistics_

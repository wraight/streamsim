
import glob
import matplotlib.pyplot as plt
import commonResultsCode as crc
import argparse
import time

thisName=__file__

######################
### parsing
######################

def GetArgs():
    parser = argparse.ArgumentParser(description=__file__)

    parser.add_argument('--strategy', help='summation strategy: ave, max, min')
    parser.add_argument('--dir', help='stats directory')
    parser.add_argument('--alt', help='use alternative to eStep for X axis (default eStep)')
    parser.add_argument('--snips', nargs='+', help='filter files')
    parser.add_argument('--args', nargs='+', help='stats to compare')

    args = parser.parse_args()

    print "args:",args

    argDict={'strategy':"ave", 'dir':"NYS", 'snips':[], 'args':[], 'alt':"eStep"}

    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict


######################
### main
######################

def main():

    argDict=GetArgs()
    statDir=argDict['dir']

    snipDicts=[]
    for a in argDict['snips']:
        found=False
        for s in snipDicts:
            if a.split(":")[0] in s['id']:
                found=True
                s['snips'].append(a.split(":")[1])
                break
        if found==False:
            snipDicts.append({ 'id':a.split(":")[0], 'snips':[a.split(":")[1]], 'files':[], 'stats':[]})
    print snipDicts

    values=[]

    rangeX=[]
    if "estep" in argDict['alt'].lower(): #as default
        rangeX=[0,1,2,5,10,20,65]
    elif "xpc" in argDict['alt'].lower():
        rangeX=[0,10,20,50]
    elif "mult" in argDict['alt'].lower():
        rangeX=[1,2,5,10,20]
    elif "var" in argDict['alt'].lower():
        rangeX=[1,2,3,4,5]
    else:
        print "no match for --alt:",argDict['alt']


    for s in snipDicts:
        for a in argDict['args']:
            values.append({'arg':a, 'cent':[], 'inc':[], 'end':[], 'snips':s['snips'], 'lab':s['id']})
            for eStep in rangeX:
                print "eStep:",eStep

                if "estep" in argDict['alt'].lower(): #as default
                    s['files']=glob.glob(statDir+'/*_es'+str(eStep)+'.*txt')
                elif "xpc" in argDict['alt'].lower():
                    s['files']=glob.glob(statDir+'/*_X'+str(eStep)+'pc*.txt')
                elif "mult" in argDict['alt'].lower():
                    s['files']=glob.glob(statDir+'/*_M'+str(eStep)+'.txt')
                elif "var" in argDict['alt'].lower():
                    s['files']=glob.glob(statDir+'/*_V'+str(eStep)+'.txt')
                else:
                    print "no match for --alt:",argDict['alt']

                print "found "+s['id']+" files in dir:",len(s['files'])
                for x in s['snips']:
                    s['files']=[ f for f in s['files'] if x in f]
                    print "filtered "+s['id']+" files:",len(s['files'])
                print "found "+s['id']+":",len(s['files'])

                for f in s['files']:
                    s['stats'].append(crc.GetStats(f))
                s['stats'].sort(key=lambda x: float(x['pos']))

                for reg in ["cent","inc","end"]:
                    treb={}
                    for st in ["min","max","ave"]:
                        treb[st]=crc.EStepVal(s['stats'], st, a, reg)
                    values[-1][reg].append(treb)
                '''
                values[-1]['cent'].append(crc.EStepVal(s['stats'], argDict['strategy'], a, "cent"))
                values[-1]['inc'].append(crc.EStepVal(s['stats'], argDict['strategy'], a, "inc"))
                values[-1]['end'].append(crc.EStepVal(s['stats'], argDict['strategy'], a, "end"))
                '''

    for v in values:
        #print('here is v...')
        print v


    plt.figure()
    for a in range(0,len(argDict['args']),1):
        count=0
        for r in ["cent","inc","end"]:
            print r
            plt.subplot2grid((len(argDict['args']), 3), (a, count))
            for l in range(0,5,1):
                for v,sty in zip(values,["-","--","-.",":"]):
                    if v['arg']==argDict['args'][a]:
                        #print('>>>this is this thing')
                        #print(v[r])
                        #plt.plot(rangeX,[x[l] for x in v[r]], crc.colz[l]+'-',linestyle=sty, label=v['lab']+"_"+str(l)+":"+"{0:0.2f}".format(max([x[l] for x in v[r]])))
                        plt.plot(rangeX,[x['ave'][l] for x in v[r]],crc.colz[l]+'--',linestyle=sty , label=v['lab']+"_"+str(l)+":"+"{0:0.2f}".format(max([x['max'][l] for x in v[r]])))
                        plt.fill_between(rangeX, [x['min'][l] for x in v[r]], [x['max'][l] for x in v[r]], alpha=0.5, facecolor=crc.colz[l], edgecolor=crc.colz[l])
                        #print v['lab']+"_"+str(l)+": "+", ".join(["{0:0.1f} ({1:0.1f}:{2:0.1f})".format(x['ave'][l],x['min'][l],x['max'][l]) for x in v[r]])
                        print v['lab']+"_"+str(l)+", "+" , ".join(["{0:0.2f}".format(x['max'][l]) for x in v[r]])
                        # 100*((x['max'][l]/v[r][0]['max'][l])-1.0)
            plt.title(r+" "+argDict['alt']+" scan: "+argDict['args'][a]+" ("+argDict['strategy']+")")
            plt.ylabel("datarate [Gb/s]") #argDict['args'][a])
            plt.xlabel(argDict['alt'])
            plt.grid(True)
            plt.legend(loc="upper right")
            count+=1

    plt.subplots_adjust(top=0.95, bottom=0.05, left=0.05, right=0.95, hspace=0.40, wspace=0.409) # plots layout
    plt.show()




if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "Total scan time: ",(end-start),"seconds"
    print "### out",__file__,"###"

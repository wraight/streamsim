#############
### compare datarate, run 5 times and check variation
#############

#############
### 50x50
#############

dataDir="output"
runArgs="--pitch 50x50 --group 5 --comp 1 --neighComp 1 --multCut 0 --eStep 1 "
statArgs="--pitch 50x50 --ind 1 --strategy max "

# extra hits
declare -a steps=("A" "B" "C" "D" "E")


## now loop through the above array
for i in "${steps[@]}"
do
  suffixArg="AdjE1"$i
  mkdir -p $dataDir$suffixArg
  python runMultiSamples.py --type b $runArgs --outDir $dataDir$suffixArg
  python globalStats.py --dir $dataDir$suffixArg --type rd53b --snip es1. $statArgs --suffix $suffixArg
  python makeIndex.py --dir html$suffixArg --snips global --suffix $suffixArg
done

return

#############
### 25x100
#############

dataDir="output"
runArgs="--pitch 25x100 --group 5 --comp 1 --neighComp 1 --eStep 1 "
statArgs="--pitch 25x100 --ind 1 --strategy max "

## now loop through the above array
for i in "${eSteps[@]}"
do
  suffixArg="NewX"$i"pc"
  mkdir -p $dataDir$suffixArg
  python runMultiSamples.py --type b --extraHits $i $runArgs --outDir $dataDir$suffixArg
  python globalStats.py --dir $dataDir$suffixArg --type rd53b --snip Xes1. $statArgs --suffix $suffixArg
  python makeIndex.py --dir html$suffixArg --snips global --suffix $suffixArg
done

return

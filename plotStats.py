
import glob
import matplotlib.pyplot as plt
import commonResultsCode as crc
import argparse

######################
### parsing
######################

def GetArgs():
    parser = argparse.ArgumentParser(description="runSamples")

    parser.add_argument('--strategy', help='summation strategy: ave, max, min')
    parser.add_argument('--dirs', nargs='+', help='stats directory')
    parser.add_argument('--snips', nargs='+', help='filter files')
    parser.add_argument('--args', nargs='+', help='stats to compare')
    parser.add_argument('--comp', help='compare LTF datarate values')
    parser.add_argument('--files', nargs='+', help='short circuit files')


    args = parser.parse_args()

    print "args:",args

    argDict={'strategy':"ave", 'dirs':[], 'snips':[], 'args':[], 'comp':0, 'files':[]  }

    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict


######################
### main
######################

def main():

    argDict=GetArgs()
    if len(argDict['dirs'])<1:
        print "no directory specified, check --dirs argument"
        return

    snipDicts=[]
    for a in argDict['snips']:
        found=False
        for s in snipDicts:
            if a.split(":")[0] in s['id']:
                found=True
                s['snips'].append(a.split(":")[1])
                break
        if found==False:
            snipDicts.append({ 'id':a.split(":")[0], 'snips':[a.split(":")[1]], 'files':[], 'stats':[]})
    print snipDicts

    for s in snipDicts:
        if len(argDict['dirs'])==1:
            s['files']=glob.glob(argDict['dirs'][0]+'/*.txt')
        else:
            for d in argDict['dirs']:
                if s['id'] in d.split(":")[0]:
                    s['files']=glob.glob(d.split(":")[1]+'/*.txt')
                    break
                else: continue
        print "found "+s['id']+" files in dir:",len(s['files'])
        for x in s['snips']:
            s['files']=[ f for f in s['files'] if x in f]
            print "filtered "+s['id']+" files:",len(s['files'])
        print "found "+s['id']+":",len(s['files'])

        for f in s['files']:
            s['stats'].append(crc.GetStats(f))
        s['stats'].sort(key=lambda x: float(x['pos']))


    plt.figure()
    for a in range(0,len(argDict['args']),1):

        if argDict['args'][a]=="special":
            plt.subplot2grid((len(argDict['args']), 3), (a, 0))
            crc.PlotValX([s['stats'] for s in snipDicts], [s['id'] for s in snipDicts], argDict['strategy'], "aveFramesPerStream", "aveStreamsPerEvent", "cent")

            plt.subplot2grid((len(argDict['args']), 3), (a, 1))
            crc.PlotValX([s['stats'] for s in snipDicts], [s['id'] for s in snipDicts], argDict['strategy'], "aveFramesPerStream", "aveStreamsPerEvent", "inc")

            plt.subplot2grid((len(argDict['args']), 3), (a, 2))
            crc.PlotValX([s['stats'] for s in snipDicts], [s['id'] for s in snipDicts], argDict['strategy'], "aveFramesPerStream", "aveStreamsPerEvent", "end")

            continue


        plt.subplot2grid((len(argDict['args']), 3), (a, 0))
        crc.PlotVal([s['stats'] for s in snipDicts], [s['id'] for s in snipDicts], argDict['strategy'], argDict['args'][a], "cent")

        plt.subplot2grid((len(argDict['args']), 3), (a, 1))
        crc.PlotVal([s['stats'] for s in snipDicts], [s['id'] for s in snipDicts], argDict['strategy'], argDict['args'][a], "inc")

        plt.subplot2grid((len(argDict['args']), 3), (a, 2))
        crc.PlotVal([s['stats'] for s in snipDicts], [s['id'] for s in snipDicts], argDict['strategy'], argDict['args'][a], "end")

    plt.subplots_adjust(top=0.95, bottom=0.05, left=0.05, right=0.95, hspace=0.40, wspace=0.409) # plots layout
    plt.show()

    if argDict['comp']==0:
        return

    print "####################"
    print "### comp..."
    print "####################"

    compDictA50={ 'l0':[3.68, 3.81, 2.07], 'l1':[0.617, 0.949, 1.07], 'l2':[1.73, 2.01, 3.07], 'l3':[1.14, 1.23, 1.78], 'l4':[0.883, 0.919, 1.21]}
    compDictB50={ 'l0':[2.6, 2.41, 1.34], 'l1':[0.408, 0.628, 0.715], 'l2':[1.09, 1.24, 1.93], 'l3':[0.632, 0.71, 1.09], 'l4':[0.435, 0.482, 0.71]}
    compDictA25={ 'l0':[3.47, 3.62, 1.98], 'l1':[0.59, 0.912, 1.02], 'l2':[1.65, 1.92, 2.94], 'l3':[1.1, 1.2, 1.72], 'l4':[0.846, 0.889, 1.17]}
    compDictB25={ 'l0':[2.22, 2.37, 1.31], 'l1':[0.38, 0.619, 0.702], 'l2':[0.98, 1.2, 1.9], 'l3':[0.594, 0.693, 1.07], 'l4':[0.42, 0.474, 0.693]}
    regArr=["cent", "inc", "end"]

    compDict=compDictA50
    for s in snipDicts:
        comp=s['stats']
        snips=[x.lower() for x in s['snips']]
        compDict={'l0':[1,1,1],'l1':[1,1,1],'l2':[1,1,1],'l3':[1,1,1],'l4':[1,1,1]}
        if "rd53a" in snips and "50x50" in snips:
            print "using rd53a 50x50 comparison"
            compDict=compDictA50
        if "rd53a" in snips and "25x100" in snips:
            print "using rd53a 25x100 comparison"
            compDict=compDictA25

        if "rd53b" in snips and "50x50" in snips:
            print "using rd53b 50x50 comparison"
            compDict=compDictB50
        if "rd53b" in snips and "25x100" in snips:
            print "using rd53b 25x100 comparison"
            compDict=compDictB25


        for l in range(0,5,1):
            print "layer: ",l
            for r in range(0,len(regArr),1):
                val=crc.GetSum([ float(c["aveDatarate"]) for c in comp if c['layer']==l and crc.CheckPos(l,float(c['pos']),regArr[r])==True ] ,argDict['strategy'])
                print "\t"+regArr[r]+" comp("+str(compDict['l'+str(l)][r])+")...{0:0.3f}".format(val)+" ... {0:0.3f}%".format(100*val/compDict['l'+str(l)][r])


if __name__ == "__main__":
    print "### in plotStats ###"
    main()
    print "### out plotStats ###"

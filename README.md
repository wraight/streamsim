# StreamSim

### Repository for python2 based simulation of RD53A and RD53B readout

Code is organised into different modules:
* [Simulating](#simulating): chip readout per position
    * RD53A sim: *rd53a_base.py*
        * per chip position: eta, layer, barrel/endcap
        * no streaming, aurora formatting
    * RD53B sim: *rd53b_base.py*
        * per chip position: eta, layer, barrel/endcap
        * inc. streaming, aurora formatting
    * multi-threading: *runMultiSamples.py*
* [Comparing](#comparing): contrast readout statistics across simulation variations
    * global plotting: *plotStats.py*
        * occupancies and derived quantities across detector: *plotArg.py*
        * stream ingredients: *plotPies.py*
    * eStep comparison: *plotEstep.py*
* [Summarising](#summarising): summarise readout statistics at each position and globally
    * html pages for individual positions: *sumStats.py*
    * html pages for global trends: *globalStats.py*

---

# Testing

`python rd53a_base.py --test 1`

Use the following settings:

| Arg | test setting |
| --- | --- |  
| trials | 100 |
| debug | 1 |
| file | os.path.expanduser("~")+"/cernbox/LTF_samples/PixelRDOAnalysis_50x50_RD53AB.root" |
| pitch | 50x50 |
| layer | 0 |
| region | barrel |
| bins | [] |
| comp | False |
| neighProb | 0.0 |
| eStep | 1 |
| empties | 1 |
| extraHits | 0 |
| test | True |

**NB** default position is z=9.0, L0, pixel=50x50

Can overwrite default test settings by including other arguments:

*e.g.* `python rd53a_base.py --test 1 --extraHits 10`

See RD53A section below for specific details of running simulation settings.

---


# Simulating

Run chip output simulations based on occupancies (from full sim.) to estimate stream statistics.
Using:
* `commonSimulationCode`: useful functions
* `rd53a_base.py`: rd53a chip simulation
* `rd53b_base.py`: rd53b chip simulation

Required python libraries:
* ROOT
* numpy

Uses (and so make sure they exist) directories `output`

---

## RD53A

> Regions are grouped into 'cores' on the chip that are addressed on the chip by 6 bits. 9 bits are needed to address the 'row' of the region, plus 1 bit for the left or right region in that address, making a total of 16 bits. Added are the 4 bits for the Time-over-Threshold (ToT) information for each of 4 pixels in a region, including zeros, making it a total of 32 bits of data per region. The region data is grouped into data frames of 64 bits, with 2 additional bits as 'Aurora frame header'. An 'event header' of 32 bit together with the first region data is the first frame of an event. A frame with no region data is transmitted for a chip without hits. With each Aurora frame containing two such 32 bit data blocks helps to recover from eventual transmission errors. The additional Aurora overhead for 'register frames' has to be taken into account for the total Aurora overhead of 4%. The resulting data size per event is multiplied by the required maximal trigger rate for a given layer, which is 1 MHz for layers 0+1 and 4 MHz for layers 2-4, respectively.

As described in [LTF paper](https://cds.cern.ch/record/2650732)

**Command**
`getParameterStats.py`

| Arg | Description (default) | e.g. |
| --- | --- | --- |
| trials | number of trials, i.e. events (1000) | 1000 |
| debug | debug option (False) | False |
| file | stats file with occupancy histos (NYS) | name_of_file |
| pitch | pixel pitch (50x50) | 50x50 |
| layer | layer (0) | 0 |
| region | region: barrel or endcap (barrel) | barrel |
| bins | specific bins ([]) | 1 5 10 |
| comp | compression toggle (False) | True |
| eStep | eStep size (0) | 1 |
| test | use test parameters (False) | True |


*E.g.*
`python rd53a_base.py --test true`

**_Comments_**


## RD53B
>The draft RD53B output format is based on the concept of data 'streams' that are concatenations of Aurora 64 bit frames... The readout chip is divided into 'cores' of 8x8 pixels (4x16) as the unit of data encoding for sensors with 50x50 (25x100) mum**2 pixels... Within each core, the bit map is constructed as a binary tree to identify all fired pixels... During the construction of the bit map there is a further compression applied for binary pairs with the tree, where a '01' is replaced by '0'. The RD53B data 'stream'.
>The RD53B data 'stream' is then assembled using the chip data from one to many events. The first bit of each 64 bit Aurora frame is reserved for the stream identification, it is 1 for the first frame of a stream, 0 for the following streams. For each event an 8 bit 'tag' data block is added containing the event header information for the first event in the 'stream', otherwise an extended 'tag' with '111' to signal a new 'tag' plus the 8 bit 'tag' information is used. Then the core column ('ccol') address is stored as 6 bits, followed by the core row address, that can be compressed and is stored as 5 or 6 bits. Next the 'bit map' for the pixel hits in the 'core' are added, followed by the 4 bit ToT information for each fired pixel. Then the information of the next core on the chip is added. If the core has the same ccol address, only the crow address is added. The stream is ended using a dynamic criteria that depends on the number of remaining empty bits that are left in the 64 bit Aurora frame after adding a core data block. If the number of empty bits is less than 'eStep' times the number of core data blocks already added to the stream, the remaining bits are left empty and a new data block is started with the 'tag' data and the next core data block. For this study, eStep= 1 is used that results in a compromised of the stream length. This parameter can be tuned in the future to adjust the average stream size for the different layers and to minimise the stream overheads. Finally, the same 4% overhead for the Aurora protocol for 'register frames' as for the RD53A protocol has to be taken into account.

As described in [LTF paper](https://cds.cern.ch/record/2650732)


some details

**_Comments_**


## Simulate multiple samples
Simulate several data streams in parallel

**Command**
`runMultiSamples.py`

| Arg | Description (default) | e.g. |
| --- | --- | --- |
| trials | number of trials | 100 |
| comp | compression toggle (False) | on |
| eStep | eStep size | 1 |
| type | rd53 chip type: a or b | a |
| group | group size for multithreading (1) | 5 |


*E.g.*
`runMultiSamples.py --trials 1000 --comp 0 --eStep 1 --group 5 --type a`

---

# Comparing


## Plot global trends for specific statistic(s)
Compare defined stream statistic across global positions

**Command**
`plotStats.py`

| Args | Comment (default) | e.g. |
| --- | --- | --- |
| strategy |  summation strategy: ave, max, min (ave) | |
| dirs | stats directories | output/ outputAlt/ |
| snips | filter files (use id:value format to distinguish plots) | plotB:rd53b plotB:es1. plotB:50x50 |
| args | stats to compare | aveDatarate |
| comp | compare stats to datarate numbers (a/b) | b |

*E.g.*
`python plotStats.py --dirs output/ --snips rdB:rd53b rdB:es1. rdB:50x50 --args aveDatarate events`

**NB** If one dirs argument given, this will be used for all files. If more than one is to be used, make sure to include the appropriate id, *e.g.*
`python plotStats.py --dirs rdA:outputA/ rdB:outputB/  --snips rdA:rd53a rdA:50x50 rdB:rd53b rdB:es1. rdB:50x50 --args aveDatarate`

## Scan eStep values
Plot maximum value across z range against eStep value

**Command**
`plotEstep.py`

| Args | Comment (default) | e.g. |
| --- | --- | --- |
| dir | stats directory | |
| snips | filter files | |
| args | stats to compare | |

*E.g.*
`python plotEstep.py --dir output/ --snips rd50B:rd53b rd50B:25x100 rd50A:rd53a rd50A:25x100 --args aveDatarate --strategy max`

---

# Summarising

HTML pages to summarise stream statistics.
Using:
* `commonResultsCode.py`: useful functions
* `sumStats.py`: make html summary of global stream statistics (option to run individual summary)
* `globalStats.py`: make html summary of stream statistics for individual positions

Required python libraries:
* time
* datetime
* glob
* matplotlib.pyplot
* argparse
* subprocess

Template markdown files: `templatePos.md` and `templateGlobal.md`
Uses (and so make sure they exist) directories `png`, `md`, `html`

## Individual summary
Summarise stream statistics for chips at specific z position
 * Stream statistics table
 * Stream ingredients table
 * Stream ingredients pie chart

**Command**
`sumStats.py`

| Args | Comment (default) | e.g. |
| --- | --- | --- |
| dir | directory for stat files (not set) | |
| type | chip type: rd53a or rd53b (rd53a) | |
| pitch | pixel pitch: 50x50 or 25x100 (50x50) | |
| layer | layer: 0-4 (0) | |
| region | barrel or endcap (barrel) | |
| pos | position in z (9) | |
| estep | eStep size (0) | |
| file | full filename to avoid building from elements (NYS) | |

*E.g.*
`python sumStats.py --dir output --type rd53a --pitch 50x50 --layer 0 --region barrel --estep 1 --pos 9`

**_Comments_**

## Global summary
Summarise stream statistics for chips across z range
* Global trend plots for each statistic
* Links to page for each z position  

**Command**
`globalStats.py`

| Args | Comment (default) | e.g. |
| --- | --- | --- |
| dir | directory for stat files (not set) | |
| type | chip type: rd53a or rd53b (rd53a) | |
| pitch | pixel pitch: 50x50 or 25x100 (50x50) | |
| snips | string(s) to help filter files (none) | |
| strategy | summaton strategy: ave, max, min (ave) | |
| ind | make individual summaries option (0) | |

*E.g.*
`python globalStats.py --dir output --type rd53a --pitch 50x50 --snip es2. --ind 1`

**_Comments_**

---

# Hacks
Not pretty bits of code. I'm not proud of them but they work.

## Add sub-region information to stat file
Add sub-region field to output stat (.txt, cvs) files. Useful for subsequent database manipulation. Sub-region key ("subReg") will be appended to top line and value will append to bottom line ("cent"/"inc"/"end").

**Command**
`addRegion.py`

| Args | Comment (default) | e.g. |
| --- | --- | --- |
| path | path to stat files ("/Users/kwraight/CERN_repositories/streamsim/testDir/") | |
| ext | extension of stat files (".txt") | |

*E.g.*
`python addRegion.py --path testDir/`

**_Comments_**
Potential to expand to append further information in the future.
**NB** For safety _copy_ interesting files to _testDir_ directory then run on copied files. Upload to DB from _testDir_.

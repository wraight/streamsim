
### code to simulate RD53A readout based on chip position: eta, layer, barrel/endcap

# No streaming
'''
    Regions are grouped into 'cores' on the chip that are addressed on the chip by 6 bits. 9 bits are needed to address the 'row' of the region, plus 1 bit for the left or right region in that address, making a total of 16 bits. Added are the 4 bits for the Time-over-Threshold (ToT) information for each of 4 pixels in a region, including zeros, making it a total of 32 bits of data per region. The region data is grouped into data frames of 64 bits, with 2 additional bits as 'Aurora frame header'. An 'event header' of 32 bit together with the first region data is the first frame of an event. A frame with no region data is transmitted for a chip without hits. With each Aurora frame containing two such 32 bit data blocks helps to recover from eventual transmission errors. The additional Aurora overhead for 'register frames' has to be taken into account for the total Aurora overhead of 4%. The resulting data size per event is multiplied by the required maximal trigger rate for a given layer, which is 1 MHz for layers 0+1 and 4 MHz for layers 2-4, respectively.
    '''

import commonSimulationCode as csc
import ROOT
import numpy as np
import os
import time


######################
### main
######################

def main():

    ######################################
    ### Commandline Arguments
    ######################################

    print csc.PrintSection("Commandline Arguments")
    argDict=csc.GetSimArgs() # to begin just get arguments
    if argDict['test']: # if test flag is set
        argDict=csc.SetTestArgs() # set test arguments
        argDict=csc.GetSimArgs(argDict) # adjust test arguments
    print "argDict:",argDict

    ######################################
    ### Simulation Settings
    ######################################

    print csc.PrintSection("Simulation Settings")
    ds= csc.DataSettings()

    ### add user settings to data settings
    if argDict['debug']: ds.debug=True
    if argDict['eStep']>0: ds.eStep=int(argDict['eStep'])
    ds.trials=argDict['trials']

    ds.PrintSettings()

    # hits per region
    hitsAve=2.0
    hitsSigma=0.5
    ######################################
    ### Get Distributions
    ######################################

    print csc.PrintSection("Get Distributions")
    ### start with looping over z positions
    regDist=[] # regions per chip

    fileName=argDict['file'].replace("50x50",argDict['pitch'])
    regName="m_h2_regions_chip_"+argDict['region']+str(argDict['layer']) # hit regions per chip

    regHist=ROOT.TH2F()
    signal=ROOT.TFile(fileName)
    signal.GetObject(regName,regHist)

    print "# N_binsX:",regHist.GetNbinsX()


    ######################################
    ### Loop over distribution bins
    ######################################

    print csc.PrintSection("Loop over distribution bins")

    for x in range(0,regHist.GetNbinsX(),1): # loop over z (x axis)

        if len(argDict['bins'])>0 and str(x) not in argDict['bins']: continue

        if ds.debug:
            print "check x:",x,"... string:",str(x)
            print "argDict bins(",len(argDict['bins']),"):",argDict['bins']

        regDist=csc.GetDistArr(regHist,x,-1)
        zpos=regHist.GetXaxis().GetBinLowEdge(x)

        if ds.debug: print "z position:", zpos
        #continue

        # bail on empty positions
        if sum(regDist)==0: continue

        regNorm=[ float(b)/sum(regDist) for b in regDist]
        '''
        regAve= sum( [n*regNorm[n] for n in range(0,len(regNorm),1)]) /sum(regNorm)
        print "### for z position",zpos,"..."
        print "# regDist("+str(sum(regDist))+"):"
        if argDict['debug']: print regDist
        print "# regNorm("+str(sum(regNorm))+"):"
        if argDict['debug']: print regNorm
        print "# ave regNorm:",regAve
        '''


        ####################################
        ### Run Simulation
        ####################################

        print csc.PrintSection("Run Simulation")

        # DataCounter
        dc=csc.DataCounter(ds, "rd53a")

        NhitPix=0
        hitRegs=[]

        # generating array of number of hit regions per event
        for e in range(0,argDict['trials'],1):
            Nregs=int(np.random.choice(range(0,len(regNorm),1), size=1, p=regNorm))
            # do NOT suppress empty chips
            #while Nregs==0: # empty chip suppression
            #    Nregs=int(np.random.choice(range(0,len(regNorm),1), size=1, p=regNorm))
            dc.regions.append(Nregs)


        # manually set hit regions in event for debug
        #hitRegs=[6]
        beginSim=True

        # loop events
        for hr in dc.regions:

            ### todo: check streaming option

            '''
            # new event tag (event header)
            if beginSim==True:
                dc.AddToCount(ds.ne,ds)
                beginSim=False
            '''
            # new event tag
            dc.AddToCount(ds.ne,ds)
            count=0


            #print csc.PrintSection("Pre-loop")
            #dc.PrintData()

            # looping over hit regions
            for r in range(0,hr,1):

                # generating randomly uniform arrays of hit region coordinates
                hrX= int(np.random.uniform(0, 400/4, 1)[0])
                hrY= int(np.random.uniform(0, 384/1, 1)[0])

                if ds.debug: print "region:",hrX,",",hrY

                dc.AddToCount(ds.add, ds)
                ''' No neighbour suppression used
                # address info.
                if np.random.uniform(0, 1, 1)[0]>argDict['neighProb']: # use neighbour address (0 ==> no neighbour suppression)
                    dc.AddToCount(ds.add, ds)
                    dc.neighboured.append(0)
                else:
                    dc.neighboured[-1]+=1
                    '''

                # ToT info.
                # 16 for RD53A#

                #regTot=GetToT(hitPixels)
                #if debug: print "ToT length:",regTot
                #dc.AddToCount({'b':regTot, 'tag':"tot"})
                dc.AddToCount(ds.tot, ds)

                '''
                    #region occupancy across z as test plot
                    #data based on 32 bit region
                    #0.5 neighbour prob (check this, make tunable)
                '''

                #close the region loop (each event)

            # extra hits: add each hit as separate region (full address+ToT)
            extraHits=int(round(hr*(argDict['extraHits']/100.0)))
            dc.Xregions.append(extraHits)
            for i in range (0,extraHits,1):
                if argDict['debug']: print "Adding",i,"of",argDict['extraHits'],"extra hits"
                dc.AddToCount(ds.add, ds)
                dc.AddToCount(ds.tot, ds)

            if len(dc.fpe)>0:
                dc.fpe.append(len(dc.frameArr)-sum(dc.fpe))
            else:
                dc.fpe.append(len(dc.frameArr))
        #close the events loop

        print csc.PrintSection("Post-loop")

        dc.TidyUp()

        if argDict['debug']:
            dc.PrintData()
            csc.PrintColourStream(dc.streams, 'b')
            dc.EndCheck()
            ds.PrintSettings()

        print csc.PrintSection("Summary: rd53a "+argDict['pitch']+" "+argDict['region']+" l:"+str(argDict['layer'])+" z:"+str(zpos))

        dc.PrintSummary(ds)

        saveName="rd53a_"+argDict['pitch']+"_"+argDict['region']+"_l"+str(argDict['layer'])+"_z"+str(zpos)+"_es"+str(argDict['eStep'])
        dSave= csc.DataSaver(os.getcwd()+"/"+argDict['outDir'],saveName)

        dSave.StatWriter(ds, zpos, dc.sumDict, argDict, saveName)

            #if argDict['debug']:
    if ds.debug==1:
        dc.PrintDict()
        dc.PrintDict("RegPerEvent")
        dc.PrintDict("FramesPerEvent")
        dc.PrintDict("BitsPerEvent")
        #print "frames per event ("+str(len(dc.fpe))+"):",dc.fpe



if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"


exit()

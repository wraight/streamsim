#
# code to decode bespoke file with hit information per event
#
import re

# read file line by line
# catch events
# flag neighbours
# get tot

fileName="/Users/kwraight/TestStream_compBTNS1_chip.txt"
dataFile=open(fileName,'r')

eventCount=0
eventArr=[]
dataArr=[]
colArr=[]
rowArr=[]

lastCol=-1
lastRow=-1

for line in dataFile:
    lineStr=str(line)
    print(lineStr)
    #print('line length: '+str(len(lineStr)))
    if len(lineStr)<2 or "Chip Tag" in lineStr: continue

    if "Event" in lineStr:
        eventCount+=1
        lastCol=-1
        lastRow=-1
    else:
        lineArr=lineStr.split('    ')
        print('linArr length: '+str(len(lineArr)))
        if len(lineArr)>7:
            #print('interesting: '+str(len(lineArr)))
            eventArr.append(eventCount)
            data=int(re.sub("[^0-9]", "",lineArr[6]))+int(re.sub("[^0-9]", "",lineArr[7]))
            dataArr.append(data)
            if int(lineArr[3])==lastCol:
                print('==column ('+str(lastCol)+'): '+str(int(lineArr[3])))
                colArr.append(1)
                if int(lineArr[4])==lastRow+1:
                    print('=row+1('+str(lastRow)+'): '+str(int(lineArr[4])))
                    rowArr.append(1)
                    lastRow=int(lineArr[4])
                else:
                    lastRow=int(lineArr[4])
                    rowArr.append(0)
            else:
                lastCol=int(lineArr[3])
                lastRow=int(lineArr[4])
                colArr.append(0)
                rowArr.append(0)


        else: print('uninteresting')


print('number of events: '+str(eventCount))
if max(eventArr)!=eventCount:
    print('event problem')
if len(dataArr)!=len(eventArr):
    print('data problem')
if len(colArr)!=len(eventArr):
    print('column problem')
if len(rowArr)!=len(eventArr):
    print('row problem')

print('number hits: '+str(len(eventArr)))

print('events:\n'+str(eventArr))
itArr=[ eventArr.count(i) for i in range(1,11,1)]
print('-->\n'+str(itArr)+"\tsum: "+str(sum(itArr)))
print('columns:\n'+str(colArr))
print('rows:\n'+str(rowArr))
print('data:\n'+str(dataArr))

#
# code to decode bespoke file with hit information per event, include bit tree builder
#

# read file line by line
# catch events
# flag neighbours
# get tot

def GetCore(eta,phi):
    return [int(eta/8), int(phi/2)]

def GetBitTree(eta,phi):

    tree=[]
    pos=[eta%8,phi%2]
    #L-R
    if pos[0]<4: tree.extend([1,0])
    else: tree.extend([0,1])
    #T-B
    if pos[1]<1: tree.extend([0,1])
    else: tree.extend([1,0])
    #L-R(2)
    if pos[0]%4<2: tree.extend([1,0])
    else: tree.extend([0,1])
    #L-R(3)
    if pos[0]%2<1: tree.extend([1,0])
    else: tree.extend([0,1])

    return tree

def AddBitTree(bta,btb):

    #bta multi hit bitTree
    #btb new single hit bitTree
    btc=[]

    for p in range(0,btb,2):
        if btb[p]==

def GetBinary(val,opt):
    if "tot" in opt.lower():
        return format(val,'04b')
    if "ccol" in opt.lower():
        return format(val,'06b')
    if "qrow" in opt.lower():
        return format(val,'08b')
    if "tree" in opt.lower():
        return bin(int(''.join(map(str, val)), 2) << 1)

    print('dunno opt: '+opt)
    return -1



fileName="/Users/kwraight/TestStream_chip.txt"
dataFile=open(fileName,'r')

eventCount=0
eventArr=[]
dataArr=[]
neighbourArr=[]

lastPos=-1

lastCore=[-1,-1]
for line in dataFile:
    lineStr=str(line)
    print(lineStr)
    #print('length: '+str(len(lineStr)))
    if len(lineStr)<2 or "Chip Tag" in lineStr: continue

    if "Event" in lineStr:
        eventCount+=1
        lastPos=-1
        lastCore=[-1,-1]
    else:
        lineArr=lineStr.split('     ')
        if len(lineArr)>0:
            #print('interesting: '+str(len(lineArr)))
            eventArr.append(eventCount)
            dataArr.append(int(lineArr[5]))
            core=GetCore(int(lineArr[1]),int(lineArr[2]))
            bitTree=GetBitTree(int(lineArr[1]),int(lineArr[2]))
            print('bit-tree('+lineArr[1]+','+lineArr[2]+'): '+str(bitTree))
            if int(lineArr[3])==lastPos:
                print('neighbours('+str(lastPos)+'): '+str(int(lineArr[3])))
                neighbourArr.append(1)
            else:
                lastPos=int(lineArr[3])
                neighbourArr.append(0)


        else: print('uninteresting')
        break

print('number of events: '+str(eventCount))
if max(eventArr)!=eventCount:
    print('event problem')
if len(dataArr)!=len(eventArr):
    print('data problem')
if len(neighbourArr)!=len(eventArr):
    print('neighbour problem')

print('number hits: '+str(len(eventArr)))

print('events:\n'+str(eventArr))
itArr=[ eventArr.count(i) for i in range(1,11,1)]
print('-->\n'+str(itArr)+"\tsum: "+str(sum(itArr)))
print('neighbours:\n'+str(neighbourArr))
print('data:\n'+str(dataArr))

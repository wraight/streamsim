#
# calculate unused bits per event based on cores hit per event
#

import matplotlib.pyplot as plt

statArr=[]
exceptArr=[]

def GetBits(cores):

    bits=1+8+ cores*(6+8+10)
    bits+= int(bits/64)*1
    unused=64-(bits%64)

    return bits, unused

for i in range(0,50,1):
    statDict={}
    statDict['cores']=i
    statDict['bits'],statDict['unused']=GetBits(i)
    statArr.append(statDict)
    if statDict['unused']>55:
        exceptArr.append(statDict)

unusedSingle=64-(1+8+6+8+10)
unusedDouble=64-(1+8+2*(6+8+10))
unusedEmpty=64-(1+8)

print(exceptArr)


plt.figure()
plt.subplot2grid((1, 2), (0, 0))
plt.plot( [s['cores'] for s in statArr], [s['bits'] for s in statArr], label="bits")
plt.title("stream size(bits) per core")
plt.ylabel("bits")
plt.xlabel("cores")
plt.grid(True)
plt.legend(loc="upper right")
plt.subplot2grid((1, 2), (0, 1))
plt.plot( [s['cores'] for s in statArr], [s['unused'] for s in statArr], label="unused")
plt.plot( [e['cores'] for e in exceptArr], [e['unused'] for e in exceptArr], 'bo')
# draw vertical line from (70,100) to (70, 250)
plt.plot([0,len(statArr)], [unusedSingle, unusedSingle], 'g--', lw=2, label="single")
plt.plot([0,len(statArr)], [unusedDouble, unusedDouble], 'b--', lw=2, label="double")
plt.plot([0,len(statArr)], [unusedEmpty, unusedEmpty], 'r--', lw=2, label="empty")
plt.title("unused bits per core")
plt.ylabel("bits")
plt.xlabel("cores")
plt.grid(True)
plt.legend(loc="upper right")

plt.show()

#
# bit-tree builder, and adder (unfinished)
#

def GetBitTree(eta,phi):

    tree=[]
    pos=[eta%2,phi%2]
    '''
    #L-R
    if pos[0]<(eDim/2): tree.extend([1,0])
    else: tree.extend([0,1])
    #T-B
    if pos[1]<(pDim/2): tree.extend([0,1])
    else: tree.extend([1,0])
    '''
    #L-R(2)
    if pos[0]%2<1: tree.extend([1,0])
    else: tree.extend([0,1])
    #L-R(3)
    if pos[1]%2<1: tree.extend([0,1])
    else: tree.extend([1,0])

    return tree

def AddBitTree(bta,btb):

    #bta multi hit bitTree
    #btb new single hit bitTree
    btc=[]

    for p in range(0,len(btb),2):
        if btb[p:p+2]==bta[p:p+2]:
            btc.extend(bta[p:p+2])
        else:
            btc.extend([sum(x) for x in zip(bta[p:p+2], btb[p:p+2])])

    return btc


for p in range(0,2,1):
    for e in range(0,2,1):
        print('hit: '+str(e)+', '+str(p))
        print('tree:'+str(GetBitTree(e,p)))

aTree=GetBitTree(0,0)
bTree=GetBitTree(0,1)

treeAB=AddBitTree(aTree, bTree)

print('combAB: '+str(treeAB))

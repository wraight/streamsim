# build a binary bit tree
import numpy as np

debug=True

def BuildBitTree( chits):
    ## 1. build empty data stream
    ## 2. fill data stream entries with hits
    ## 3. remove empty pairs
    
    doubles=0
    bitList=[]
    # bit pair format: TB, LR
    
    ## 1. build empty data stream
    for step in range(0,6,1):
        stepList=[]
        for a in range(0,np.power(2,step),1): stepList.extend([0,0])
        #if debug: print step,"stepList("+str(len(stepList))+"):","".join([str(s) for s in stepList])
        bitList.extend(stepList)
    
    ## 2. fill data stream entries with hits
    size=8
    for c in chits:
        if debug: print "cx, cy:",c[0],",",c[1]
        ## could boil this down to a loop but for clarity leave unpacked
        
        ## y split --> 2 halves
        offset=0
        coord=c[1]/(size/2)
        bitList[coord +offset]=1 # 1st pair of bits, mod half size
        #if debug: print "bt_0("+str(len(bitList))+"):","".join([str(b) for b in bitList])
        
        ## x split --> 4 quadrants
        offset=2
        coord=c[0]/(size/2) +2*coord
        bitList[coord +offset]=1 # 2nd 2 pair bits, mod half size plus offset
        #if debug: print "bt_1("+str(len(bitList))+")","".join([str(b) for b in bitList])
        
        ## y split --> 8 parts
        offset=6
        coord=(c[1]%(size/2))/(size/4) +2*coord
        bitList[coord +offset]=1 # 2nd 2 pair bits, mod half size plus offset
        #if debug: print "bt_2("+str(len(bitList))+")","".join([str(b) for b in bitList])
        
        ## x split 16 parts
        offset=14
        coord=(c[0]%(size/2))/(size/4) +2*coord
        bitList[coord +offset]=1 # 2nd 2 pair bits, mod half size plus offset
        #if debug: print "bt_3("+str(len(bitList))+")","".join([str(b) for b in bitList])
        
        ## y split 32 parts
        offset=30
        coord=(c[1]%(size/4))/(size/8) +2*coord
        bitList[coord +offset]=1 # 2nd 2 pair bits, mod half size plus offset
        #if debug: print "bt_4("+str(len(bitList))+")","".join([str(b) for b in bitList])
        
        ## x split 64 parts
        offset=62
        coord=(c[0]%(size/4))/(size/8) +2*coord
        if bitList[coord +offset]==1:
            print "!!! this is a double entry !!!" # check for doubles (only valid at this step) I think this is simply due to limited random method used to generate hit positions
            doubles+=1
            if debug: print chits
        bitList[coord +offset]=1 # 2nd 2 pair bits, mod half size plus offset
        if debug: print "bt_5("+str(len(bitList))+")","".join([str(b) for b in bitList])
    
    ## 3. remove empty pairs
    sparseList= []
    for pair in range(0,126,2):
        if bitList[pair:pair+2]!=[0,0]: sparseList.extend(bitList[pair:pair+2])
    if debug: print "sparse("+str(len(sparseList))+")","".join([str(b) for b in sparseList])

    return sparseList, doubles

#hX= int(np.random.uniform(0, 8, 1)[0])
#hY= int(np.random.uniform(0, 8, 1)[0])

BL=[0,0]
BR=[0,7]
TR=[7,7]

sList,dubs=BuildBitTree([BL])
print sList

sList,dubs=BuildBitTree([BL,BR])
print sList

sList,dubs=BuildBitTree([BL,TR])
print sList

sList,dubs=BuildBitTree([BL,BR,TR])
print sList





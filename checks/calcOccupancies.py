#
# calculate chip occupancies which include zero-bin correction
#
import ROOT


def Ave(arr):
    ave=  float( sum([ a * float(arr[a])/sum(arr) for a in range(0,len(arr),1)] ))
    return ave


def GetDistArr(hist, x, ybins):

    distArr=[] # array for y bin distribution
    #check how many y bins to include (to suppress long tails if necessary)
    nbins=ybins
    if nbins<0:
        nbins=hist.GetNbinsY()
    #loop over y bins to fill dist array
    for y in range(0,nbins,1):
        distArr.append(hist.GetBinContent(x+1,y+1))

    return distArr

def ZeroBinCorrection(arr, region, layer, pos):

    posSum=-1
    arrSum=sum(arr)
    if arr[0]>0:
        print "### WARNING: NO zero-bin correction ... array zero-bin already contentful:",arr[0]
    else:
        if "barrel" in region.lower():
            chipsPerZ=[16*1,20*4,32*4,44*4,56*4]
            posSum=chipsPerZ[layer]
            if layer==0 and pos>240.0:
                posSum=20
        elif "endcap" in region.lower():
            chipsPerZ=[28*1,20*4,32*4,44*4,52*4]
            posSum=chipsPerZ[layer]
        else:
            print " ### WARNING: NO zero-bin correction ... don't understand region:",region
        arr[0]=posSum*100-sum(arr)
        print "### WARNING: adding zero-bin content using "+str(posSum*100)+","+str(arrSum)+":",arr[0]

    return arr

def CheckPos(l,pos,reg):

    incArr=[245, 245, 372, 372, 372]
    endArr=[1065, 1569, 1145, 1145, 1145]
    if "all" in reg.lower():
        return True
    elif "flat" in reg.lower() or "cent" in reg.lower():
        if pos>incArr[l]: return False
        else: return True
    elif "inc" in reg.lower():
        if l==0 and pos in [1064, 1187, 1325]: return False
        if l==0 and pos in [1122, 1252, 1399]: return True
        if pos<=incArr[l] or pos>endArr[l]: return False
        else: return True
    elif "end" in reg.lower():
        if l==0 and pos in [1064, 1187, 1325]: return True
        if l==0 and pos in [1122, 1252, 1399]: return False

        if pos<endArr[l]: return False
        else: return True
    else:
        return False

fileName="/Users/kwraight/Cernbox/LTF_samples/PixelRDOAnalysis_50x50_RD53AB.root"
signal=ROOT.TFile(fileName)

chipsPerZ=[16*1,20*4,32*4,44*4,56*4]
#chipsPerZ=[28*1,20*4,32*4,44*4,52*4]

region="barrel" # barrel or endcap

for l in range(0,5,1):
    print "\n*** layer",l

    corePerChipName="m_h2_cores_chip_"+region+str(l)
    hitsPerCoreName="m_h2_hits_core_"+region+str(l)
    corePerChip=ROOT.TH2F()
    hitsPerCore=ROOT.TH2F()
    signal.GetObject(corePerChipName,corePerChip)
    signal.GetObject(hitsPerCoreName,hitsPerCore)

    regPerChipName="m_h2_regions_chip_"+region+str(l)
    hitsPerRegName="m_h2_hits_region_"+region+str(l)
    regPerChip=ROOT.TH2F()
    hitsPerReg=ROOT.TH2F()
    signal.GetObject(regPerChipName,regPerChip)
    signal.GetObject(hitsPerRegName,hitsPerReg)

    hitsPerChipName="m_h_hits_chip_"+region+str(l)
    hitsPerChip=ROOT.TH1F()
    signal.GetObject(hitsPerChipName,hitsPerChip)

    maxCore=-1
    maxCorePos=-1
    maxCore0=-1
    maxCorePos0=-1
    maxReg=-1
    maxRegPos=-1

    count=0
    centCheck=False
    for x in range(0,corePerChip.GetNbinsX(),1):

        corePerChipDist=GetDistArr(corePerChip,x,-1)
        if sum(corePerChipDist)==0: continue

        zpos=corePerChip.GetXaxis().GetBinLowEdge(x)

        if region=="barrel" and centCheck==False and CheckPos(l,zpos,"cent")==False:
            print "central part info..."
            print "hist:",hitsPerChip.GetMean()
            print "cores:",maxCore,"@",maxCorePos
            print "regions:",maxReg,"@",maxRegPos
            print "corrected:",maxCore0,"@",maxCorePos0
            maxCore=-1
            maxCorePos=-1
            maxCore0=-1
            maxCorePos0=-1
            maxReg=-1
            maxRegPos=-1
            centCheck=True


        hitsPerCoreDist=GetDistArr(hitsPerCore,x,-1)
        regPerChipDist=GetDistArr(regPerChip,x,-1)
        hitsPerRegDist=GetDistArr(hitsPerReg,x,-1)

        corePerChipAve=Ave(corePerChipDist)
        hitsPerCoreAve=Ave(hitsPerCoreDist)
        regPerChipAve=Ave(regPerChipDist)
        hitsPerRegAve=Ave(hitsPerRegDist)
        '''
        print "cores("+str(x)+"):\t",corePerChipAve,"*",hitsPerCoreAve,"=",corePerChipAve*hitsPerCoreAve
        print "regions("+str(x)+"):\t",regPerChipAve,"*",hitsPerRegAve,"=",regPerChipAve*hitsPerRegAve
        '''
        if corePerChipAve*hitsPerCoreAve>maxCore:
            maxCore=corePerChipAve*hitsPerCoreAve
            maxCorePos=zpos
        if regPerChipAve*hitsPerRegAve>maxReg:
            maxReg=regPerChipAve*hitsPerRegAve
            maxRegPos=zpos

        corePerChipDist0=ZeroBinCorrection(corePerChipDist,region,l,zpos)
        corePerChipAve0=Ave(corePerChipDist0)
        '''
        print "corrected("+str(x)+"):\t",corePerChipAve0,"*",hitsPerCoreAve,"=",corePerChipAve0*hitsPerCoreAve
        '''
        if corePerChipAve0*hitsPerCoreAve>maxCore0:
            maxCore0=corePerChipAve0*hitsPerCoreAve
            maxCorePos0=zpos

    print "hist:",hitsPerChip.GetMean()
    print "cores:",maxCore,"@",maxCorePos
    print "regions:",maxReg,"@",maxRegPos
    print "corrected:",maxCore0,"@",maxCorePos0

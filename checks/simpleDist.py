#
# code to compare sampling
#


import commonSimulationCode as csc
import ROOT
import numpy as np
import matplotlib.pyplot as plt
import os
import time
import copy


######################
### main
######################

def main():
    ######################################
    ### Get Distributions
    ######################################

    print csc.PrintSection("Get Distributions")
    ### start with looping over z positions
    regDist=[] # regions per chip

    regName="m_h2_cores_chip_barrel4" # hit regions per chip

    regHist=ROOT.TH2F()
    signal=ROOT.TFile(os.path.expanduser("~")+"/cernbox/LTF_samples/PixelRDOAnalysis_50x50_RD53AB.root")
    signal.GetObject(regName,regHist)

    x=1023

    regDist=csc.GetDistArr(regHist,x,-1)
    zpos=regHist.GetXaxis().GetBinLowEdge(x)

    print "z position:", zpos

    if sum(regDist)==0:
        print "*** no content here"
        return

    regNorm=[ float(b)/sum(regDist) for b in regDist]
    regAve= sum( [n*regNorm[n] for n in range(0,len(regNorm),1)]) /sum(regNorm)
    print "### for z position",zpos,"..."
    print "# max regDist("+str(len(regDist))+"):",max(regDist),"@",regDist.index(max(regDist))
    #print regDist
    print "# sum regNorm("+str(len(regNorm))+"):",sum(regNorm)
    #if argDict['debug']: print regNorm
    print "# ave regNorm:",regAve

    ### zero-bin correction
    regDist2=copy.deepcopy(regDist)
    regDist2=csc.ZeroBinCorrection(regDist2,"barrel",4,zpos)
    print "orig("+str(len(regDist))+"):", regDist[0]
    print "copy("+str(len(regDist2))+"):", regDist2[0]
    regNorm2=[ float(b)/sum(regDist2) for b in regDist2]

    NregsA=np.random.choice(range(0,len(regNorm),1), size=10000, p=regNorm)
    NregsA=[int(a) for a in NregsA]

    NregsB=[]

    for e in range(0,10000,1):
        Nregs=int(np.random.choice(range(0,len(regNorm2),1), size=1, p=regNorm2))
        NregsB.append(Nregs)

    print "ave A:",float(sum(NregsA))/len(NregsA)
    print "ave B100:",float(sum(NregsB[0:100]))/len(NregsB[0:100])
    print "ave B1000:",float(sum(NregsB[0:1000]))/len(NregsB[0:1000])
    print "ave B10000:",float(sum(NregsB[0:10000]))/len(NregsB[0:10000])

    plt.figure()
    plt.plot(range(0,len(regDist),1),regNorm, color="r",label="root")
    plt.plot(range(0,len(regDist),1),regNorm2, color="g",label="root2",linestyle="--")

    plt.hist(NregsA, histtype="step", color="b",label="A",normed=True)
    plt.hist(NregsB[0:100], histtype="step", color="g",label="B100",normed=True)
    plt.hist(NregsB[0:1000], histtype="step", color="m",label="B1000",normed=True)
    plt.hist(NregsB[0:10000], histtype="step", color="k",label="B10000",normed=True)

    plt.grid(True)
    plt.legend(loc="upper right")

    plt.show()



if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"


exit()

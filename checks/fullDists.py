#
# plot distributions from profile in root file
#
import ROOT
import matplotlib.pyplot as plt


colz=['b','r','y','g','m']


def GetBits(cores):

    bits=1+8+ cores*(6+8+10)
    bits+= int(bits/64)*1
    unused=64-(bits%64)

    return bits, unused

def CheckPos(l,pos,reg):

    incArr=[245, 245, 372, 372, 372]
    endArr=[1065, 1569, 1145, 1145, 1145]
    if "all" in reg.lower():
        return True
    elif "flat" in reg.lower() or "cent" in reg.lower():
        if pos>incArr[l]: return False
        else: return True
    elif "inc" in reg.lower():
        if l==0 and pos in [1064, 1187, 1325]: return False
        if l==0 and pos in [1122, 1252, 1399]: return True
        if pos<=incArr[l] or pos>endArr[l]: return False
        else: return True
    elif "end" in reg.lower():
        if l==0 and pos in [1064, 1187, 1325]: return True
        if l==0 and pos in [1122, 1252, 1399]: return False

        if pos<endArr[l]: return False
        else: return True
    else:
        return False

fileName="/Users/kwraight/Cernbox/LTF_samples/RD53BEncoding_50x50_baseline.root"
signal=ROOT.TFile(fileName)
'''
m_p_cores_per_chip_barrel
m_p_hits_per_core_barrel
m_p_datarate_per_chip_barrel
m_p_streamlength_per_stream_barrel
m_p_cores_per_stream_barrel
m_p_orphans_per_stream_barrel
m_p_tot_per_stream_barrel
m_p_bittree_per_stream_barrel
m_p_tags_per_stream_barrel
m_p_addressing_per_stream_barrel
'''
#branchName="RD53BUncompressedBitTree"
branchName="encodingCompBTNS1"
profName="m_p_orphans_per_stream_barrel"

centTotArr=[]
incTotArr=[]
endTotArr=[]

for l in range(0,5,1):
    barProfName=branchName+"/"+profName+str(l) # hit regions per chip
    endProfName=branchName+"/"+profName.replace("barrel","endcap")+str(l) # hit regions per chip

    barProf=ROOT.TProfile()
    endProf=ROOT.TProfile()
    signal.GetObject(barProfName,barProf)
    signal.GetObject(endProfName,endProf)
    centProfArr=[]
    incProfArr=[]
    endProfArr=[]

    for x in range(0,barProf.GetNbinsX(),1): # loop over z (x axis)

        cont=barProf.GetBinContent(x+1)
        bits,unused=GetBits(cont)
        if cont==0:
            continue
        valX=barProf.GetXaxis().GetBinLowEdge(x)
        #print('cont '+str(valX)+': '+str(cont))
        if CheckPos(l,valX,"cent"):
            centProfArr.append({'x':valX, 'y':cont, 'bits':bits, 'unused':unused})
        else:
            incProfArr.append({'x':valX, 'y':cont, 'bits':bits, 'unused':unused})

    for x in range(0,endProf.GetNbinsX(),1): # loop over z (x axis)

        cont=endProf.GetBinContent(x+1)
        bits,unused=GetBits(cont)
        if cont==0:
            continue
        valX=endProf.GetXaxis().GetBinLowEdge(x)
        #print('cont '+str(valX)+': '+str(cont))

        endProfArr.append({'x':valX, 'y':cont, 'bits':bits, 'unused':unused})

    centTotArr.append(centProfArr)
    incTotArr.append(incProfArr)
    endTotArr.append(endProfArr)


plt.figure()
count=0
yVal='y'
for arr,reg in zip([centTotArr, incTotArr, endTotArr], ["cent","inc","end"]):
    plt.subplot2grid((1, 3), (0, count))
    for l in range(0,5,1):
        plt.plot( [a['x'] for a in arr[l]], [a[yVal] for a in arr[l]], colz[l], label="l_"+str(l)+":"+"{0:0.2f}".format(max([a[yVal] for a in arr[l]])))
        print('l_'+str(l)+': '+str(max([a[yVal] for a in arr[l]])))
    count+=1
    plt.title(profName+"_"+reg)
    plt.ylabel(yVal)
    plt.xlabel("z[mm]")
    plt.grid(True)
    plt.legend(loc="upper right")

plt.subplots_adjust(top=0.95, bottom=0.05, left=0.05, right=0.95, hspace=0.40, wspace=0.409) # plots layout
plt.show()




print('layer:\t'+'\t'.join([str(l) for l in range(0,5,1)]))
print('cent:\t'+'\t'.join( [ "{:.2f}".format(max([a[yVal] for a in centTotArr[l]])) for l in range(0,5,1) ] ) )
print('inc:\t'+'\t'.join( [ "{:.2f}".format(max([a[yVal] for a in incTotArr[l]])) for l in range(0,5,1) ] ) )
print('end:\t'+'\t'.join( [ "{:.2f}".format(max([a[yVal] for a in endTotArr[l]])) for l in range(0,5,1) ] ) )

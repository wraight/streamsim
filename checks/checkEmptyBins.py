#
# output bin content
#
import ROOT



fileName="/Users/kwraight/Cernbox/LTF_samples/PixelRDOAnalysis_50x50_RD53AB.root"
signal=ROOT.TFile(fileName)

chipsPerZ=[16*1,20*4,32*4,44*4,56*4]
#chipsPerZ=[28*1,20*4,32*4,44*4,52*4]


for l in range(0,5,1):
    print "\n*** layer",l

    regType="regions" # regions or cores
    histName="m_h2_"+regType+"_chip_barrel"+str(l)
    myHist=ROOT.TH2F()
    signal.GetObject(histName,myHist)

    maxSum=-1
    maxSumPos=-1
    maxEmpty=-1
    maxEmptyPos=-1

    count=0
    for i in range(0,myHist.GetNbinsX(),1):
        colSum=0
        for j in range(0,myHist.GetNbinsY(),1):
            colSum+= myHist.GetBinContent(i+1,j+1)
        if colSum>0:
            count+=1
            empty=chipsPerZ[l]*100-colSum
            if l==0 and i>240:
                empty=20*100-colSum
            print "x="+str(myHist.GetXaxis().GetBinLowEdge(i))+"("+str(i)+") sum: "+str(colSum)+" ... zeros: "+str(empty)
            if colSum>maxSum:
                maxSum=colSum
                maxSumPos=i
            if empty>maxEmpty:
                maxEmpty=empty
                maxEmptyPos=i

    print "maxSum("+str(maxSum)+") at i="+str(maxSumPos)
    print "maxEmpty("+str(maxEmpty)+") at i="+str(maxEmptyPos)

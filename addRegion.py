# some hack code to add region field to output stat Files
# useful for subsequent database manipulation

import argparse
import os


######################
### parsing
######################

def GetArgs(ad=None):
    parser = argparse.ArgumentParser(description="addRegion")

    parser.add_argument('--path', help='path to stat files (default "/Users/kwraight/CERN_repositories/streamsim/testDir/")')
    parser.add_argument('--ext', help='extension of stat files (default ".txt")')
    parser.add_argument('--clean', help='remove unformatted files (default "0")')

    args = parser.parse_args()

    print "args:",args

    argDict={}
    if ad==None:
        argDict={'path':"/Users/kwraight/CERN_repositories/streamsim/testDir/", 'ext':".txt", 'clean':0}
    else:
        argDict=ad

    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict

##################################
# Useful Functions
##################################

def GetFiles(path,ext):

    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        for file in f:
            if ext in file:
                files.append(os.path.join(r, file))
    print "Found",len(files),"in",path

    return files


def GetStats(fileName):

    argDict={}
    names=[]
    vals=[]
    file = open(fileName, 'r')
    for line in file:
        if "pos" in line: names=line.split("\t")
        else: vals=line.split("\t")

    for n,v in zip(names,vals):
        if n=="\n": continue
        try:
            argDict[n]=int(v)
        except:
            argDict[n]=v
    file.close()

    return argDict


def CheckPos(l,pos,reg):

    incArr=[245.0, 245.0, 372.0, 372.0, 372.0]
    #endArr=[1065.0, 1569.0, 1145.0, 1145.0, 1145.0]
    endArr=[1065.0, 1568.0, 1144.0, 1144.0, 1144.0]

    if "barrel" in reg.lower():
        # R0 filter
        if l==0 and pos in [1064, 1187, 1325]: return "end"
        if l==0 and pos in [1122, 1252, 1399]: return "inc"
        # inclined cut
        if pos>incArr[l]: return "inc"
        else: return "cent"

    elif "end" in reg.lower():
        # R0 filter
        if l==0 and pos in [1064, 1187, 1325]: return "end"
        if l==0 and pos in [1122, 1252, 1399]: return "inc"
        # endcap cut
        if pos<endArr[l]: return "NYS"
        else: return "end"

    else:
        return "NYS"


def AddBit(fileName):

    argDict=GetStats(fileName)
    try:
        subReg=CheckPos(int(argDict['layer']),float(argDict['pos']),argDict['region'])
    except:
        print "Problem file:",fileName
        for key in ['layer', 'pos', 'region']:
            try: print key,":",argDict[key]
            except KeyError: print "problem with",key
        return "dirty"

    if "NYS" in subReg:
        print subReg,"found"
        print int(argDict['layer']),",",float(argDict['pos']),",",argDict['region']

    if subReg==None:
        print "+++Problem with identifying subReg"
        print int(argDict['layer']),float(argDict['pos']),argDict['region']
        return

    fin = open(fileName, "r")
    data = "NYS"
    for line in fin:
        if "pos" in line:
            data = line.replace('\n','subReg\t\n')
            #data = line.replace('subReg\n','subReg\t\n')
        else:
            #data = line.replace('\n'+'\tsubReg\n')
            data += line+str(subReg)+'\t'
            #data += line.replace(str(subReg)+'\n',str(subReg)+'\t\n')

        data = data.replace('\t\t','\t')
    fin.close()

    fin = open(fileName, "wt")
    fin.write(data)
    fin.close()

    return "clean"


######################
### main
######################

def main():

    argDict=GetArgs()
    if len(argDict['path'])<1:
        print "no directory specified, check --path argument"
        return

    #Step 1: a)find files
    myFiles= GetFiles(argDict['path'], argDict['ext'])

    #Step 2: a)add extra info to file
    cleaned=0
    for f in myFiles:
        retVal=AddBit(f)
        if "dirty" in retVal and argDict['clean']:
            os.remove(f)
            print "removed",f
            cleaned+=1

    print "Files cleaned:",cleaned

if __name__ == "__main__":
    print "### in addRegion ###"
    main()
    print "### out addRegion ###"

#############
### 50x50
#############

suffixArg="NYS"
dataDir="output" #$suffixArg"
runArgs="--pitch 50x50 --group 5 " #"--extraHits 10"
statArgs="--pitch 50x50 --ind 1 --strategy max --suffix $suffixArg"

python runMultiSamples.py --type a --eStep 0 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53a --snip es0. $statArgs

python runMultiSamples.py --type b --eStep 0 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53b --snip es0. $statArgs

python runMultiSamples.py --type b --eStep 1 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53b --snip es1. $statArgs

python runMultiSamples.py --type b --eStep 2 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53b --snip es2. $statArgs

python runMultiSamples.py --type b --eStep 5 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53b --snip es5. $statArgs

python runMultiSamples.py --type b --eStep 10 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53b --snip es10. $statArgs

python runMultiSamples.py --type b --eStep 20 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53b --snip es20. $statArgs

python runMultiSamples.py --type b --eStep 65 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53b --snip es65. $statArgs

#############
### 25x100
#############

runArgs="--pitch 25x100 --group 5 "#"--extraHits 10"
statArgs="--pitch 25x100 --ind 1 --strategy max --suffix $suffixArg"

python runMultiSamples.py --type a --eStep 0 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53a --snip es0. $statArgs

python runMultiSamples.py --type b --eStep 0 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53b --snip es0. $statArgs

python runMultiSamples.py --type b --eStep 1 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53b --snip es1. $statArgs

python runMultiSamples.py --type b --eStep 2 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53b --snip es2. $statArgs

python runMultiSamples.py --type b --eStep 5 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53b --snip es5. $statArgs

python runMultiSamples.py --type b --eStep 10 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53b --snip es10. $statArgs

python runMultiSamples.py --type b --eStep 20 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53b --snip es20. $statArgs

python runMultiSamples.py --type b --eStep 65 $runArgs --outDir $dataDir
python globalStats.py --dir $dataDir --type rd53b --snip es65. $statArgs

python makeIndex.py --dir html$suffixArg --snips global --suffix $suffixArg

import subprocess
import argparse
import threading
import time
import os

######################
### parsing
######################

def GetArgs():
    parser = argparse.ArgumentParser(description=__file__)

    # pass on
    parser.add_argument('--trials', help='number of trials')
    parser.add_argument('--comp', help='compression toggle')
    parser.add_argument('--neighComp', help='column neighbour compression toggle')
    parser.add_argument('--empties', help='toggle count empty chips')
    parser.add_argument('--eStep', help='eStep size')
    parser.add_argument('--multCut', help='stop stream after N mulitple events')
    parser.add_argument('--pitch', help='pixel pitch')
    parser.add_argument('--extraNoise', help='extra noise hits')
    parser.add_argument('--extraHits', help='extra track hits')
    parser.add_argument('--outDir', help='output directory')
    parser.add_argument('--layer', help='particular layer (debugging)')
    parser.add_argument('--binary', help='suppress tot info., bit-tree only (rd53b only)')
    # keep here
    parser.add_argument('--adjust', help='run adjusted test script')
    parser.add_argument('--group', help='group size for multithreading')
    parser.add_argument('--type', help='rd53 a or b')
    parser.add_argument('--test', help='run single')

    args = parser.parse_args()

    print "args:",args

    argDict={'trials':1000, 'adjust': 0, 'layer':-1, 'binary':False, 'comp':False, 'neighComp': 0, 'empties':1, 'eStep':1, 'multCut':-1, 'test':0, 'pitch':"50x50", 'group':1, 'type':"ab", 'file':os.path.expanduser("~")+"/Cernbox/LTF_samples/RD53BEncoding_50x50_baseline.root", 'extraNoise':0, 'extraHits':0 }

    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]


    return argDict


######################################
### useful functions
######################################

class myThread (threading.Thread):
    def __init__(self, threadID, name, arr):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.arr = arr
    def run(self):
        print "@@@ Starting " + self.name
        run_sample(self.name, self.arr)
        print "@@@ Exiting " + self.name

def run_sample(threadName, arr):
    print "### runMultiSamples.runsample: command: "," ".join(arr)
    subprocess.check_call(arr)

def LoopOverDetector(commandIn, group, testOpt=False, layer=-1):

    layers=range(0,5,1)
    if layer>-1: layers=[layer]
    regions=["barrel","endcap"]

    global threadArr
    global threadCount

    for r in regions:
        for l in layers:

            if testOpt and threadCount>0: continue

            #commandArr=[ "echo","\"dummy command\" ",str(threadCount)]
            commandArr=[ c for c in commandIn]
            commandArr.extend(["--layer",str(l),"--region",r])
            thread1= myThread(threadCount, "Thread_"+"_l"+str(l), commandArr)
            threadCount+=1
            thread1.start()
            threadArr.append(thread1)
            print "job ",threadCount
            print "modulo:",threadCount%group
            if threadCount%group==0:
                print "ready to run"
                for t in threadArr:
                    t.join()
                print "reset array"
                threadArr=[]


######################
### main
######################
threadLock = threading.Lock()
threadArr = []
threadCount = 0

def main():

    argDict=GetArgs()
    print "argDict:",argDict

    ### RD53A
    if "a" in argDict['type'].lower():
        commandArr=["python","rd53a_base.py","--trials",str(argDict['trials']),"--eStep",str(argDict['eStep']),"--multCut",str(argDict['multCut']),"--neighComp",str(argDict['neighComp']),"--comp",str(argDict['comp']),"--empties",str(argDict['empties']),"--pitch",argDict['pitch'],"--file",argDict['file'],"--extraHits",str(argDict['extraHits']),"--outDir",argDict['outDir']]
        LoopOverDetector(commandArr, argDict['group'], argDict['test'], argDict['layer'])


    ### RD53B
    if "b" in argDict['type'].lower():
        if argDict['adjust']:
            commandArr=["python","rd53b_test.py","--trials",str(argDict['trials']),"--eStep",str(argDict['eStep']),"--multCut",str(argDict['multCut']),"--neighComp",str(argDict['neighComp']),"--binary",str(argDict['binary']),"--comp",str(argDict['comp']),"--empties",str(argDict['empties']),"--pitch",argDict['pitch'],"--file",argDict['file'],"--extraHits",str(argDict['extraHits']),"--extraNoise",str(argDict['extraNoise']),"--outDir",argDict['outDir']]
        else:
            commandArr=["python","rd53b_base.py","--trials",str(argDict['trials']),"--eStep",str(argDict['eStep']),"--multCut",str(argDict['multCut']),"--neighComp",str(argDict['neighComp']),"--binary",str(argDict['binary']),"--comp",str(argDict['comp']),"--empties",str(argDict['empties']),"--pitch",argDict['pitch'],"--file",argDict['file'],"--extraHits",str(argDict['extraHits']),"--extraNoise",str(argDict['extraNoise']),"--outDir",argDict['outDir']]
        LoopOverDetector(commandArr, argDict['group'], argDict['test'], argDict['layer'])



if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"

###
### make html summary of stream statistics for individual positions
###
import glob
import matplotlib.pyplot as plt
import commonResultsCode as crc
import argparse
import subprocess
import time

######################
### parsing
######################

def GetArgs():
    parser = argparse.ArgumentParser(description=__file__)

    parser.add_argument('--dir', help='directory for stat files')
    parser.add_argument('--type', help='chip type: rd53a or rd53b')
    parser.add_argument('--pitch', help='pixel pitch: 50x50 or 25x100')
    parser.add_argument('--layer', help='layer: 0-4')
    parser.add_argument('--region', help='region: barrel or endcap')
    parser.add_argument('--pos', help='position in z')
    parser.add_argument('--estep', help='eStep size')
    parser.add_argument('--file', help='full filename to avoid building')

    args = parser.parse_args()
    
    print "args:",args
    
    argDict={'dir':"output", 'type':"RD53A", 'pitch':"50x50", 'layer':0, 'region':"barrel", 'estep':0, 'pos':9, 'file':"NYS" }
    
    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]
    
    return argDict

######################
### main
######################

def main():

    argDict=GetArgs()
    
    commandStr="python sumStats.py "+" ".join(["--"+str(k)+" "+str(v) for k,v, in argDict.items()])
    
    fileName=crc.FindFile(argDict)
    markName=fileName
    markName=markName.replace("output","md")
    markName=markName.replace("txt","md")
    stats=[]
    if "NYS" not in fileName:
        stats=crc.GetStats(fileName)
        print stats
        crc.FillTemplatePos(fileName, commandStr, stats, markName)
    print fileName

    htmlName=markName.replace("md","html")
    print subprocess.check_output(['multimarkdown', markName, '-o', htmlName])

if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"

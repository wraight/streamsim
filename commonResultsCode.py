###
### common code for analysis of stream statistics
###
import matplotlib.pyplot as plt
import os
import datetime


######################################
### useful functions
######################################

def FindFile(argDict):

    fileName=argDict['file'] # assume file set
    if "NYS" in fileName: # if fileName unset then build it
        fileName=argDict['dir']
        if fileName[-1]!="/":
            fileName=fileName+"/"
        arr= [ argDict['type'], argDict['pitch'], argDict['region'], "l"+str(argDict['layer']), "z"+str(argDict['pos'])+".0", "es"+str(argDict['estep']) ]
        fileName=fileName+"_".join([a.lower() for a in arr])
    if fileName[-4::]!=".txt":
        fileName=fileName+".txt"

    print "SDF:",fileName
    exists = os.path.isfile(fileName) # check file exists
    if exists:
        return fileName
    else:
        return "NYS"


colz=['b','r','y','g','m']

def GetSum(arr,sumi):
    num=-1
    if "ave" in sumi or "mean" in sumi:
        try:
            num=float(sum(arr))/len(arr)
        except ZeroDivisionError:
            num=-1
    if "max" in sumi:
        try:
            num=max(arr)
        except ValueError:
            num=-1
    if "min" in sumi:
        try:
            num=min(arr)
        except ValueError:
            num=-1
    if "sum" in sumi:
        num=sum(arr)

    return num


def GetStats(fileName):

    argDict={}
    names=[]
    vals=[]
    file = open(fileName, 'r')
    for line in file:
        if "pos" in line: names=line.split("\t")
        else: vals=line.split("\t")

    for n,v in zip(names,vals):
        if n=="\n": continue
        try:
            argDict[n]=int(v)
        except:
            argDict[n]=v

    return argDict

def CheckPos(l,pos,reg):

    incArr=[245, 245, 372, 372, 372]
    endArr=[1065, 1569, 1145, 1145, 1145]
    if "all" in reg.lower():
        return True
    elif "flat" in reg.lower() or "cent" in reg.lower():
        if pos>incArr[l]: return False
        else: return True
    elif "inc" in reg.lower():
        if l==0 and pos in [1064, 1187, 1325]: return False
        if l==0 and pos in [1122, 1252, 1399]: return True
        if pos<=incArr[l] or pos>endArr[l]: return False
        else: return True
    elif "end" in reg.lower():
        if l==0 and pos in [1064, 1187, 1325]: return True
        if l==0 and pos in [1122, 1252, 1399]: return False

        if pos<endArr[l]: return False
        else: return True
    else:
        return True

def EStepVal(arr, strat, val, reg="all"):

    retVals=[]
    for l in range(0,5,1):
        print "layer",l
        infoArr=[ float(c[val]) for c in arr if c['layer']==l and CheckPos(l,float(c['pos']),reg)==True ]
        print infoArr
        sumVal=GetSum( infoArr ,strat)
        retVals.append(sumVal)

    return retVals

def BandVal(arr, val, reg="all"):

    retVals=[]
    for l in range(0,5,1):
        print "layer",l
        infoArr=[ float(c[val]) for c in arr if c['layer']==l and CheckPos(l,float(c['pos']),reg)==True ]
        print infoArr
        sumVal=GetSum( infoArr ,strat)
        retVals.append(sumVal)

    return retVals


def PlotVal(arrs, labs, strat, val, reg="all"):

    styles=["-","--","-.",":"]
    retStr=reg+":\t"#\t".join(labs)+"\n"
    for l in range(0,5,1):
        count=0
        #retStr+=reg+"\t"
        for arr,lab in zip(arrs,labs):
            sumVal=GetSum( [ float(c[val]) for c in arr if c['layer']==l and CheckPos(l,float(c['pos']),reg)==True ] ,strat)
            plt.plot( [float(c['pos']) for c in arr if c['layer']==l and CheckPos(l,float(c['pos']),reg)==True], [float(c[val]) for c in arr if c['layer']==l and CheckPos(l,float(c['pos']),reg)==True], colz[l]+'-', label=lab+str(l)+":{0:.2f}".format(sumVal), linestyle=styles[count])
            retStr+="{0:.2f}\t".format(sumVal)
            count+=1
    retStr+="\n\n"
    plt.title(val+": "+reg+" ("+strat+")")
    plt.ylabel(val)
    plt.xlabel("z[mm]")
    plt.grid(True)
    plt.legend(loc="upper right")
    return retStr


def PlotValX(arrs, labs, strat, val1, val2, reg="all"):

    styles=["-","--","-.",":"]
    retStr=reg+":\t"#\t".join(labs)+"\n"
    for l in range(0,5,1):
        count=0
        #retStr+=reg+"\t"
        for arr,lab in zip(arrs,labs):
            sumVal=GetSum( [ float(c[val1])*float(c[val2]) for c in arr if c['layer']==l and CheckPos(l,float(c['pos']),reg)==True ] ,strat)
            plt.plot( [float(c['pos']) for c in arr if c['layer']==l and CheckPos(l,float(c['pos']),reg)==True], [float(c[val1])*float(c[val2]) for c in arr if c['layer']==l and CheckPos(l,float(c['pos']),reg)==True], colz[l]+'-', label=lab+str(l)+":{0:.2f}".format(sumVal), linestyle=styles[count])
            retStr+="{0:.2f}\t".format(sumVal)
            count+=1
    retStr+="\n\n"
    plt.title("special: "+reg+" ("+strat+")")
    plt.ylabel("special")
    plt.xlabel("z[mm]")
    plt.grid(True)
    plt.legend(loc="upper right")
    return retStr


def MakePieChart(arr, name, title="PieChart"):

    print "\n### PieMaker: title...",title

    fig, ax = plt.subplots(figsize=(6, 3), subplot_kw=dict(aspect="equal"))

    wedges, texts, autotexts = ax.pie([i*100 for i in arr.values()], autopct='%1.1f%%', colors=['blue','grey','red','green'])

    ax.legend(wedges, arr.keys(), title="Ingredients", loc="center left", bbox_to_anchor=(1, 0, 0.5, 1))

    plt.setp(autotexts, size=8, weight="bold")

    ax.set_title(title)

    plt.subplots_adjust(top=0.95, bottom=0.05, left=0.05, right=0.95, hspace=0.40, wspace=0.409) # plots layout

    svNm=name
    if ".png" not in svNm:
        svNm+=".png"

    fig.savefig(svNm, bbox_inches='tight')

    #print "saving:",svNm

    return svNm


def FillTemplatePos(posStr, commandStr, statArr, newName,tempName="templatePos.md"):


    ### get template markdown to make new page
    tempFile = open(tempName, 'r')
    contents="NYS"
    if tempFile.mode == 'r':
        contents=tempFile.read()
    tempFile.close()

    ### date string
    dateStr=datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    print "running on:",dateStr

    ### making table of stream statistics
    statTableStr=""
    ingVals={}
    useVals={}
    for k, v in statArr.items():
        statTableStr+="| "+str(k.replace(">>","").replace("<<",""))+" | "+str(v)+" |\n"
        if ">>" in k:
            ingVals[k.replace(">>","")]=v
        if "<<" in k:
            useVals[k.replace("<<","")]=v


    ### ingredients pie chart
    ingPieName=MakePieChart(ingVals, newName.replace(".md","_ingPie.png").replace("md/","png/"), statArr['layerPos'])
    print "ingPieName:",ingPieName
    usePieName=MakePieChart(useVals, newName.replace(".md","_usePie.png").replace("md/","png/"), statArr['layerPos'])
    print "usePieName:",usePieName

    ### ingredients table
    infoTableStr="| ![usePieChart]("+str(os.getcwd())+"/"+usePieName+") || ![ingPieChart]("+str(os.getcwd())+"/"+ingPieName+") ||\n"
    infoTableStr+="| occupancy | prop(%) | ingredient | prop(%) |\n"
    infoTableStr+="| --- | --- | --- | --- |\n"
    for i in range(0,len(ingVals.items()),1):
        try:
            infoTableStr+="| "+useVals.items()[i][0]+" | "+str(float(100*useVals.items()[i][1])/sum(useVals.values()))+" | "
        except IndexError:
            infoTableStr+="| | | "
        infoTableStr+=ingVals.items()[i][0]+" | "+str(float(100*ingVals.items()[i][1])/sum(ingVals.values()))+" |\n"

    ### template --> particular
    contents=contents.replace("somePosition", posStr)
    contents=contents.replace("someDate", dateStr)
    contents=contents.replace("someDir", os.getcwd())
    contents=contents.replace("_someCommand_", commandStr)
    contents=contents.replace("_info_", infoTableStr)
    contents=contents.replace("_statistics_", statTableStr)


    print "writing:", newName
    newFile =open(newName, "w")
    newFile.write(contents)
    newFile.close()

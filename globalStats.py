###
### make html summary of global stream statistics (option to run individual summary)
###
import glob
import matplotlib.pyplot as plt
import commonResultsCode as crc
import argparse
import subprocess
import os
import datetime
import time
import threading

######################
### parsing
######################

def GetArgs():
    parser = argparse.ArgumentParser(description=__file__)

    parser.add_argument('--dir', help='directory for stat files')
    parser.add_argument('--type', help='chip type: rd53a or rd53b')
    parser.add_argument('--pitch', help='pixel pitch: 50x50 or 25x100')
    parser.add_argument('--snips', nargs='+', help='filter files')
    parser.add_argument('--suffix', help='output directories suffix')
    parser.add_argument('--strategy', help='summation strategy: ave, max, min')
    parser.add_argument('--ind', help='make individual summaries')
    parser.add_argument('--group', help='group size for multithreading')

    args = parser.parse_args()

    print "args:",args

    argDict={'dir':"output", 'suffix':"NYS", 'type':"RD53A", 'pitch':"50x50", 'snips':[], 'strategy': "ave", "group":5, 'ind':0}

    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    return argDict

######################################
### useful functions
######################################

class myThread (threading.Thread):
    def __init__(self, threadID, name, arr):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.arr = arr
    def run(self):
        print "@@@ Starting " + self.name
        run_sample(self.name, self.arr)
        print "@@@ Exiting " + self.name

def run_sample(threadName, arr):
    print "### runMultiSamples.runsample: command: "," ".join(arr)
    subprocess.check_call(arr)

######################
### main
######################

def main():

    argDict=GetArgs()
    statDir=argDict['dir']

    commandStr="python globalStats.py "+" ".join(["--"+str(k)+" "+str(v) for k,v, in argDict.items()])

    htmlStr="html"
    mdStr="md"
    pngStr="png"
    if "NYS" not in argDict['suffix']:
        htmlStr+=argDict['suffix']
        mdStr+=argDict['suffix']
        pngStr+=argDict['suffix']
    if not os.path.exists(htmlStr):
        os.makedirs(htmlStr)
    if not os.path.exists(mdStr):
        os.makedirs(mdStr)
    if not os.path.exists(pngStr):
        os.makedirs(pngStr)


    markName=mdStr+"/"+argDict['type'].lower()+'_'+argDict['pitch'].lower()+"_"+"_".join([a.lower() for a in argDict['snips']])+"_global.md"
    htmlName=markName.replace("md","html")

    ### find files in folder
    # all files
    sim_files = glob.glob(statDir+'/*'+argDict['type'].lower()+'*'+argDict['pitch'].lower()+'*.txt')
    print "found "+argDict['type'].lower()+" "+argDict['pitch'].lower()+":",len(sim_files)
    if len(sim_files)<1:
        print "not files found matching:",statDir+'/*'+argDict['type'].lower()+'*'+argDict['pitch'].lower()+'*.txt'
    # subset based on snips
    for s in argDict['snips']:
        sim_files=[ f for f in sim_files if s.lower() in f]
        print "remaining with '"+s.lower()+"':",len(sim_files)

    if len(sim_files)<1:
        print "not files found matching"
        return

    # individual stats
    if argDict['ind']!=0:
        threadLock = threading.Lock()
        threads = []
        count=0
        for s in sim_files:
            commandArr=['python', 'sumStats.py', '--file', s]
            thread1= myThread(count, "Thread_"+str(count), commandArr)
            count+=1
            thread1.start()
            #print subprocess.check_output(['python', 'sumStats.py', '--file', s])
            threads.append(thread1)
            if count%argDict['group']==0:
                for t in threads:
                    t.join()
                threads=[]

    ### individual links
    linksStr="Positions:\n\n"
    linkArr=[[],[],[],[],[]]
    for s in sim_files:
        link=s.replace(argDict['dir'],htmlStr)
        link=link.replace("txt","html")
        stats=crc.GetStats(s)
        linkArr[int(stats['layer'])].append("["+stats['pos']+"]("+os.getcwd()+"/"+link+")")
    for l in range(0,len(linkArr),1):
        linkArr[l].sort(key=lambda x:float(x[x.find("[")+1:x.find("]")]))
        linksStr+="**layer"+str(l)+"**: "+"\t".join(linkArr[l])+"\n\n"

    ### get individual stats
    simStats=[]
    for f in sim_files:
        simStats.append(crc.GetStats(f))
    simStats.sort(key=lambda x: float(x['pos']))

    ### plotting global stats
    lab=argDict['type'][-1]+"_"+argDict['pitch'][0:2]+"_"
    figsStr=""


    for s in simStats[0]:
        if "pos" in s.lower() or "region" in s.lower() or "command" in s.lower():
            print "skipping", s
            continue

        figStatStr="\n\nlayer:\t"+"\t".join([str(r) for r in range(0,5,1)])+"\n\n"
        plt.figure(figsize=(12,3))
        plt.subplot2grid((1, 3), (0, 0))
        figStatStr+=crc.PlotVal([simStats], [lab], argDict['strategy'], s, "cent")
        plt.subplot2grid((1, 3), (0, 1))
        figStatStr+=crc.PlotVal([simStats], [lab], argDict['strategy'], s, "inc")
        plt.subplot2grid((1, 3), (0, 2))
        figStatStr+=crc.PlotVal([simStats], [lab], argDict['strategy'], s, "end")
        plt.subplots_adjust(top=0.95, bottom=0.05, left=0.05, right=0.95, hspace=0.40, wspace=0.409) # plots layout

        ### save (png) figures
        figName= pngStr+"/"+argDict['type'].lower()+"_"+argDict['pitch'].lower()+"_"+"_".join(argDict['snips'])+"_"+s+".png"
        plt.savefig(figName, bbox_inches='tight')
        figsStr+="![fig:"+s+"]("+str(os.getcwd())+"/"+figName+") \n\n"
        figsStr+=figStatStr
        plt.close()

    ### get template markdown to make new page
    tempFile = open("templateGlobal.md", 'r')
    contents="NYS"
    if tempFile.mode == 'r':
        contents=tempFile.read()
    tempFile.close()

    ### position string
    posStr=argDict['type'].lower()+'_'+argDict['pitch'].lower()+" ("+",".join(argDict['snips'])+")"

    ### date string
    dateStr=datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    print "running on:",dateStr

    ### template --> particular
    contents=contents.replace("somePosition", posStr)
    contents=contents.replace("someDate", dateStr)
    contents=contents.replace("someDir", os.getcwd())
    contents=contents.replace("_someCommand_", commandStr)
    contents=contents.replace("_someLinks_", linksStr)
    contents=contents.replace("_plots_", figsStr)

    ### write markdown file
    newFile =open(markName, "w")
    newFile.write(contents)
    newFile.close()

    ### generate html from markdown
    print subprocess.check_output(['multimarkdown', markName, '-o', htmlName])
    print "wrote:",htmlName

if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "Total scan time: ",(end-start),"seconds"
    print "### out",__file__,"###"


### common functions for simulation

import numpy as np
import matplotlib.pyplot as plt
import argparse
import ROOT
import os
import collections


######################
### parsing
######################

def GetSimArgs(ad=None):

    parser = argparse.ArgumentParser(description="rd53 simulation")

    parser.add_argument('--trials', help='number of trials')
    parser.add_argument('--debug', help='debug')
    parser.add_argument('--outDir', help='output directory')
    parser.add_argument('--file', help='stats file with histos')
    parser.add_argument('--pitch', help='pixel pitch')
    parser.add_argument('--layer', help='layer')
    parser.add_argument('--region', help='region: barrel or endcap')
    parser.add_argument('--bins', nargs='+', help='specific bins')
    parser.add_argument('--binary', help='suppress tot info., bit-tree only (rd53b only)')
    parser.add_argument('--comp', help='compression toggle (rd53b only)')
    parser.add_argument('--neighComp', help='column compression toggle (rd53b only)')
    parser.add_argument('--eStep', help='eStep size')
    parser.add_argument('--multCut', help='stop stream after N mulitple events')
    parser.add_argument('--empties', help='include empty chips (on by default)')
    parser.add_argument('--extraHits', help='number of uncorrelated track-like hits to add per event (percent)')
    parser.add_argument('--extraNoise', help='number of uncorrelated noise-like hits to add per event (percent)')
    parser.add_argument('--test', help='use test parameters')

    args = parser.parse_args()

    print "args:",args

    argDict={}
    if ad==None:
        argDict={'trials':1000, 'debug':0, 'outDir':"output", 'file':"NYS",  'pitch':"50x50", 'layer':-1,  'region':"barrel", 'bins':[], 'binary':False, 'comp':False, 'eStep':1, 'multCut':-1, 'neighComp': 0, 'empties':1, 'extraNoise':0, 'extraHits':0, 'test':False}
    else:
        argDict=ad

    for a in vars(args).iteritems():
        if not a[1]==None:
            print "got argument",a
            try:
                argDict[a[0]]=int(a[1])
            except:
                argDict[a[0]]=a[1]

    if argDict['layer']<0 or argDict['layer']>4:
        print "l value out of bounds:",argDict['layer'],"Setting layer to 0."
        argDict['layer']=0

    return argDict

### test arguments for debugging/development
def SetTestArgs():
    argDict={'trials':100, 'debug':1, 'outDir':"output", 'file':os.path.expanduser("~")+"/Cernbox/LTF_samples/RD53BEncoding_50x50.root",  'pitch':"50x50", 'layer':0,  'region':"barrel", 'bins':[], 'binary':False, 'comp':False,  'neighComp':0, 'eStep':1, 'multCut':-1, 'empties':1, 'extraNoise':0, 'extraHits':0, 'test':True}
    return argDict


def PrintSection(txt):
    print "\n######################################"
    print "###",txt
    print "######################################\n"
    return

######################################
### standard settings
######################################

class DataSettings():
    def __init__(self):
        self.afs = 64
        self.debug = False
        self.trials = 1
        #datarate scales
        self.over = 1+0.04
        self.trig = [1,1,4,4,4]
        #rd53a
        self.ne = {'b':32,'tag':"ne_tag"} # new event flag
        self.add = {'b':16, 'tag':"reg_add"} # region address
        self.tot = {'b':16, 'tag':"reg_tot"} # region tot (4 bits per pixel)
        #rd53b
        self.ns = {'b':1,'tag':"ns_tag"} # new stream flag
        self.tagX = {'b':8, 'tag':"X_tag"} # tag: For each event an 8 bit 'tag' data block is added containing the event header information for the first event in the 'stream'
        self.tagY = {'b':11, 'tag':"Y_tag"} # tag: otherwise an extended 'tag' with '111' to signal a new 'tag' plus the 8 bit 'tag' information is used.
        #stream step
        self.eStep = 0
        self.multCut = 0


    def Get(self, name):
    ## get class attribute info
        if self.__dict__.has_key(name):
            return self.__dict__[name]
        else:
            print "# DataSettings.Get: no such",name
            return

    def GetTag(self, name):
    ## get class attribute tag
        if self.__dict__.has_key(name):
            try:
                return self.__dict__[name]['tag']
            except TypeError:
                print "# DataSettings.GetTag: no such tag for",name
                return
        else:
            print "# DataSettings.GetTag: no such",name
            return

    def GetBits(self, name):
    ## get class attribute bits
        if self.__dict__.has_key(name):
            try:
                return self.__dict__[name]['b']
            except TypeError:
                print "# DataSettings.GetBits: no such tag for",name
                return
        else:
            print "# DataSettings.GetBits: no such",name
            return

    def PrintSettings(self):
        # settings
        members = [attr for attr in dir(self) if not callable(getattr(self, attr)) and not attr.startswith("__")]
        print "\n### Data settings..."
        for m in members: print "# "+m+": "+str(self.Get(m))


class DataCounter():
    def __init__(self, ds, chip="NYS"):
        self.frame=[] # N bits in size (set by DS afs (default 64))
        if "rd53b" in chip.lower():
            self.frame=[ds.ns, ds.tagX]
        self.frameArr=[] # collection of frames --> stream, controlled by estep
        self.streams=[] # collection of streams
        self.eVal=0
        #
        self.regions=[]
        self.Xregions=[]
        self.hits=[]
        self.eStepsPerStream=[]
        #
        self.pieDict={}
        self.sumDict={}
        self.colShare=[0]
        self.rowShare=[0]
        self.empties=0
        self.fpe=[] # frames per event
        self.eCount=0
        self.events=0

    def FrameBitView(self, fr):
        return [f['b'] for f in fr]

    def FrameTagView(self, fr):
        return [f['tag'] for f in fr]

    def SplitChunk(self, chunk, ds):
        size=chunk['b']
        if ds.debug: print "# DataCounter.SplitChunk: pre size: ", size
        while size>ds.afs-sum([f['b'] for f in self.frame]):
            size-=ds.afs-sum([f['b'] for f in self.frame])
            if ds.debug: print "# DataCounter.SplitChunk: size: ", size
            self.frame.append({'b':ds.afs-sum([f['b'] for f in self.frame]), 'tag':str(chunk['tag']+"+")})
            self.frameArr.append(self.frame)
            self.frame=[ds.ns]
        if size>0:
            if ds.debug: print "# DataCounter.SplitChunk: last size: ", size
            self.frame.append({'b':size, 'tag':chunk['tag']+"+"})

    def CheckFrameSpace(self, chunk, ds): # make sure there's space in the frame for data

        frameSum=sum([f['b'] for f in self.frame])

        ### debug info.
        if ds.debug: print "# DataCounter.CheckFrameSpace: frame: ", self.FrameBitView(self.frame)
        # if current bits > frameSize then cannot add
        if frameSum>ds.afs:
            print "### WARNING: DataCounter.CheckFrameSpace: frame OVER size >afs("+str(ds.afs)+"):",sum([f['b'] for f in self.frame])
            return False
        # if current bits + bits to add > frameSize then cannot add
        if chunk['b']+frameSum>ds.afs:
            if ds.debug: print "### DEBUG: DataCounter.CheckFrameSpace: {:0} TOO BIG to add to current ({:1})".format(chunk['b'],sum([f['b'] for f in self.frame]))
            self.SplitChunk(chunk,ds)
            return False
        # add bits if space in frame
        if chunk['b']+frameSum<=ds.afs:
            if ds.debug: print "### DEBUG: DataCounter.CheckFrameSpace: ADD {:0} to CURRENT ({:1})".format(chunk['b'],sum([f['b'] for f in self.frame]))
            return True
        # check something happened
        print "### WARNING: DataCounter.CheckFrameSpace: NOTHING happened here"
        return False


    def SetNewFrame(self, chunk, ds, arrOpt=True): # start a new frame in stream. Add bits if stream is not new
        frameSum=sum([f['b'] for f in self.frame])
        ### debug output
        if ds.debug: print "# DataCounter.SetNewFrame: new frame"
        if ds.debug: print "### DEBUG DataCounter.SetNewFrame old frame("+str(frameSum)+"):",self.FrameBitView(self.frame)," ",(ds.afs-frameSum),"bits remaining"
        if ds.debug: print "### DEBUG DataCounter.SetNewFrame new frame to add with ",chunk['b']," bits"
        #store current frame to array before  resetting (ignore if setting up stream)
        if arrOpt==True:
            self.frameArr.append(self.frame)
        self.frame=[ds.ns, chunk]

        return

    def SetNewStream(self, ds): # start a new stream
        if ds.debug: print "# DataCounter.SetNewStream: old stream frame sizes("+str(len(self.frameArr))+")",[ sum([c['b'] for c in f]) for f in self.frameArr]
        #store current stream array
        self.streams.append(self.frameArr)
        self.frameArr=[]

        return self.SetNewFrame( ds.tagX, ds, False) #new frame with flag + tag bits (opt:do not add to frame array)

    def CheckEval(self, ds): # check stream to continue by E value check
        frameSum=sum([f['b'] for f in self.frame])
        # if current bits in frame less than eVal then ready to end stream
        if ds.debug: print "# DataCounter.CheckEval ("+str(self.eVal)+") frame("+str(frameSum)+"): ", self.FrameBitView(self.frame)
        if (ds.afs-frameSum)<self.eVal:
            if ds.debug: print "### DEBUG DataCounter.CheckEval triggered: Stream End"
            self.frameArr.append(self.frame) # add current frame as last to stream
            self.SetNewStream(ds)
            self.eStepsPerStream.append(self.eVal)
            self.eVal=0
            self.eCount+=1
            return False
        else: return True


    def AddToCount(self, chunk, ds):
        bits=-1
        if ds.debug: print "### DEBUG DataCounter.AddToCount add to count:",chunk
        try:
            bits=chunk['b']
        except keyError:
            print "### ERROR DataCounter.AddToCount no bits found in chunk:",chunk
            return -1
        if sum([f['b'] for f in self.frame])==64:
            self.frameArr.append(self.frame)
            self.frame=[ds.ns]
        if self.CheckFrameSpace(chunk, ds): #add if space
            self.frame.append(chunk)
        #else: #start new frame
        #    self.SetNewFrame(chunk,ds)

    def MultChop(self, ds):
        if ds.debug: print "\n### DataCounter.MultChop..."
        self.frameArr.append(self.frame)
        self.streams.append(self.frameArr)
        self.frameArr=[]
        self.SetNewFrame( ds.tagX, ds, False)


    def TidyUp(self, ds):
        if ds.debug: print "\n### DataCounter.TidyUp..."
        if collections.Counter(self.FrameTagView(self.frame)) == collections.Counter(["ns_tag","X_tag"]):
            print('excess frame')
        else:
            self.frameArr.append(self.frame)
        self.frame=[]
        if len(self.frameArr)>0:
            self.streams.append(self.frameArr)
        else:
            print('empty frameArr')
        self.frameArr=[]


    def PrintData(self):
        # data info.
        print "\n### DataCounter.PrintData..."
        members = [attr for attr in dir(self) if not callable(getattr(self, attr)) and not attr.startswith("__")]
        print vars(self)


    def PrintDict(self, snip="NYS"):
        # data info.
        print "\n### DataCounter.PrintDict, snip:",snip
        for k,v in self.sumDict.items():
            if "NYS" not in snip and snip.lower() not in k.lower(): continue
            else:
                print "entry:",k,":",v


    def EndCheck(self):
        print "\n### DataCounter.EndCheck..."
        print "bits on last frame("+str(sum(self.FrameBitView(self.frame)))+"):",self.FrameBitView(self.frame)
        print "frames in last array("+str(len(self.frameArr))+"):",[sum(self.FrameBitView(fa)) for fa in self.frameArr]
        print "stream total:",len(self.streams)


    def PrintSummary(self,ds):
        print "\n### DataCounter.Summary..."
        print "ave regions per event(max,min): "+str(float(sum(self.regions))/len(self.regions))+"("+str(max(self.regions))+","+str(min(self.regions))+")"+" # empties: "+str(self.empties)
        totFrames= sum( [ len(fa) for fa in self.streams] )
        totMaxBits= totFrames*ds.afs
        totUsedBits= sum( [ sum( [ sum(self.FrameBitView(f)) for f in fa]) for fa in self.streams] )
        totUNusedBits= totMaxBits-totUsedBits
        print "---------------"
        print "total streams:",len(self.streams)
        print "total frames in stream:",totFrames, "... maxBits:",totMaxBits
        print "-------"
        print "total used bits in stream:",totUsedBits, "bitOcc: {0:0.2f}".format(float(totUsedBits)/totMaxBits)
        print "average bits per frame: {0:0.2f}".format(float(totUsedBits)/totFrames)
        print "-------"
        print "total UNused bits in stream:",totUNusedBits, "bitEff: {0:0.2f}".format(float(totUNusedBits)/totMaxBits)
        print "average UNused bits per frame: {0:0.2f}".format(float(totUNusedBits)/totFrames)
        print "---------------"
        self.sumDict["<<"+"usedBits"]=totUsedBits
        self.sumDict["<<"+"UNusedBits"]=totUNusedBits
        for tag in ["add","tot","tag"]:
            tagBits= sum( [ sum( [ sum( [ c['b'] for c in f if tag in c['tag'] ] ) for f in fa]) for fa in self.streams] )
            print "total",tag,"bits in stream:",tagBits, "bitOcc: {0:0.2f}".format(float(tagBits)/totUsedBits)
            self.pieDict[tag]=tagBits
            self.sumDict[">>"+tag]=tagBits
        print "---------------"
        print "average bits per event:",float(totMaxBits)/ds.trials
        self.sumDict['events']=len(self.regions)
        self.sumDict['empties']=self.empties
        # event regions
        self.sumDict['aveRegPerEvent']=float(sum(self.regions))/self.sumDict['events']
        self.sumDict['minRegPerEvent']=min(self.regions)
        self.sumDict['maxRegPerEvent']=max(self.regions)
        self.sumDict['aveXRegPerEvent']=float(sum(self.Xregions))/self.sumDict['events']
        self.sumDict['minXRegPerEvent']=min(self.Xregions)
        self.sumDict['maxXRegPerEvent']=max(self.Xregions)
        # event hits (not kept at the moment)
        self.sumDict['aveHitsPerEvent']=-1
        self.sumDict['maxHitsPerEvent']=-1
        # event frames
        self.sumDict['aveFramesPerEvent0']=float( sum( [ len(fa) for fa in self.streams] ))/self.sumDict['events']
        self.sumDict['aveFramesPerEvent']=float( sum( self.fpe ))/self.sumDict['events']
        self.sumDict['minFramesPerEvent']=min(self.fpe )
        self.sumDict['maxFramesPerEvent']=max(self.fpe )
        # event bits
        self.sumDict['aveBitsPerEvent']=ds.afs*self.sumDict['aveFramesPerEvent']
        self.sumDict['minBitsPerEvent']=ds.afs*self.sumDict['minFramesPerEvent']
        self.sumDict['maxBitsPerEvent']=ds.afs*self.sumDict['maxFramesPerEvent']
        # frame used bits
        self.sumDict['maxUsedBitsPerFrame']=max( [ max( [ sum(self.FrameBitView(f)) for f in fa]) for fa in self.streams] )
        self.sumDict['minUsedBitsPerFrame']=min( [ min( [ sum(self.FrameBitView(f)) for f in fa]) for fa in self.streams] )
        self.sumDict['maxChunkPerFrame']=max( [ max( [ max( [ c['b'] for c in f  ]  ) for f in fa]) for fa in self.streams] )
        self.sumDict['minChunkPerFrame']=min( [ min( [ min( [ c['b'] for c in f  ]  ) for f in fa]) for fa in self.streams] )
        self.sumDict['aveUsedBitsPerFrame']=ds.afs*float(sum( [ sum( [ sum(self.FrameBitView(f)) for f in fa]) for fa in self.streams] ))/sum( [ sum( [ ds.afs for f in fa]) for fa in self.streams])
        self.sumDict['aveUNsedBitsPerFrame']=ds.afs*float(sum( [ sum( [ ds.afs-sum(self.FrameBitView(f)) for f in fa]) for fa in self.streams] ))/sum( [ sum( [ ds.afs for f in fa]) for fa in self.streams])
        # event used bits
        self.sumDict['aveUsedBitsPerEvent']=float(sum( [ sum( [ sum(self.FrameBitView(f)) for f in fa]) for fa in self.streams] ))/self.sumDict['events']
        self.sumDict['fracUsedBitsPerEvent']=float(sum( [ sum( [ float(sum(self.FrameBitView(f)))/ds.afs for f in fa]) for fa in self.streams] ))/self.sumDict['events']
        # event unused bits
        self.sumDict['aveUnusedBitsPerEvent']=float(sum( [ sum( [ ds.afs-sum(self.FrameBitView(f)) for f in fa]) for fa in self.streams] ))/self.sumDict['events']
        self.sumDict['fracUnusedBitsPerEvent']=float(sum( [ sum( [ float(ds.afs-sum(self.FrameBitView(f)))/ds.afs for f in fa]) for fa in self.streams] ))/self.sumDict['events']
        # event data fracs
        self.sumDict['fracTotBitsPerEvent']=float(sum( [ sum( [ sum( [ float(c['b'])/ds.afs for c in f if "tot" in c['tag'] ] ) for f in fa]) for fa in self.streams] ))/self.sumDict['events']
        self.sumDict['fracAddBitsPerEvent']=float(sum( [ sum( [ sum( [ float(c['b'])/ds.afs for c in f if "add" in c['tag'] ] ) for f in fa]) for fa in self.streams] ))/self.sumDict['events']
        self.sumDict['fracTagBitsPerEvent']=float(sum( [ sum( [ sum( [ float(c['b'])/ds.afs for c in f if "tag" in c['tag'] ] ) for f in fa]) for fa in self.streams] ))/self.sumDict['events']
        # stream frames
        self.sumDict['streams']=len(self.streams)
        self.sumDict['frames']=sum( [ len(fa) for fa in self.streams])
        self.sumDict['aveStreamsPerEvent']=float(len(self.streams))/self.sumDict['events']
        self.sumDict['aveFramesPerStream']=float(sum( [ len(fa) for fa in self.streams] ))/len(self.streams)
        self.sumDict['minFramesPerStream']=min( [ len(fa) for fa in self.streams] )
        self.sumDict['maxFramesPerStream']=max( [ len(fa) for fa in self.streams] )
        # stream bits
        self.sumDict['aveBitsPerStream']=float(sum( [ len(fa)*ds.afs for fa in self.streams] ))/len(self.streams)
        self.sumDict['minBitsPerStream']=min( [ len(fa)*ds.afs for fa in self.streams] )
        self.sumDict['maxBitsPerStream']=max( [ len(fa)*ds.afs for fa in self.streams] )
        if len(self.eStepsPerStream)>0:
            self.sumDict['aveEstepsPerStream']=float(len( self.eStepsPerStream ))/len(self.streams)
            self.sumDict['minEstepsPerStream']=min( self.eStepsPerStream )
            self.sumDict['maxEstepsPerStream']=max( self.eStepsPerStream )
        if len(self.colShare)>0:
            self.sumDict['aveColShare']=float(sum( self.colShare ))/len(self.colShare)
        else:
            self.sumDict['colShare']=-1
        if len(self.rowShare)>0:
            self.sumDict['aveRowShare']=float(sum( self.rowShare ))/len(self.rowShare)
        else:
            self.sumDict['rowShare']=-1
        self.sumDict['eCount']=self.eCount
        # datarate
        self.sumDict['aveDatarateNoTrig']=float( self.sumDict['aveFramesPerEvent'] *(ds.afs) *ds.trig[0]*ds.over)/1000.0
        self.sumDict['aveDatarateNoTrig2']=float( self.sumDict['frames'] *(ds.afs) *ds.trig[0]*ds.over)/(self.sumDict['events']*1000.0)






def ColourText(toCol, colCode):
    col=30

    #rd53a
    if "ne" in colCode: col=36 # cyan
    elif "tag" in colCode: col=34 # blue
    elif "add" in colCode: col=32 # green
    elif "data" or "tot" in colCode: col=31 # red

    #rd53b
    if "ns" in colCode: col=36 # cyan
    elif "X" in colCode: col=34 # blue
    elif "Y" in colCode: col=34 # blue
    elif "cc" in colCode: col=35 # purple
    elif "cr" in colCode: col=35 # purple
    elif "add" in colCode: col=32 # green
    elif "data" or "tot" in colCode: col=31 # red

    else: col=30
    return "\033[1;"+str(col)+"m"+str(toCol)+"\033[m"

def PrintColourStream(stream, opt='b'):

    if "all" in opt: return ", ".join([ str(x) for x in self.streams])
    else:
        streamStr="["
        for fa in stream:
            for f in fa:
                if sum([c['b'] for c in f])>64: streamStr+="\t"
                for c in f:
                    streamStr+=ColourText(c[opt],c['tag'])+", "
                streamStr+="' "
                if sum([c['b'] for c in f])>64: streamStr+="\t"
        streamStr+="]"
    print streamStr




######################################
### useful functions
######################################


def GetDistArr(hist, x, ybins):

    distArr=[] # array for y bin distribution
    #check how many y bins to include (to suppress long tails if necessary)
    nbins=ybins
    if nbins<0:
        nbins=hist.GetNbinsY()
    #loop over y bins to fill dist array
    for y in range(0,nbins,1):
        distArr.append(hist.GetBinContent(x+1,y+1))

    return distArr


def ZeroBinCorrection(arr, region, layer, pos, sampleSize):

    posSum=-1
    arrSum=sum(arr)
    if arr[0]>0:
        print "### WARNING: NO zero-bin correction ... array zero-bin already contentful:",arr[0]
    else:
        if "barrel" in region.lower():
            chipsPerZ=[16*1,20*4,32*4,44*4,56*4]
            posSum=chipsPerZ[layer]
            if layer==0 and pos>240.0:
                posSum=20
        elif "endcap" in region.lower():
            chipsPerZ=[28*1,20*4,32*4,44*4,52*4]
            posSum=chipsPerZ[layer]
        else:
            print " ### WARNING: NO zero-bin correction ... don't understand region:",region
        arr[0]=posSum*sampleSize-sum(arr)
        print "### WARNING: adding zero-bin content using "+str(posSum*100)+","+str(arrSum)+":",arr[0]

    return arr



def GetToT(hits): # this is used to mimic some compression of ToT info
    # sanity check
    if hits>4: "### WARNING: hits exceeds 4"
    #region hitmap
    #assuming some sort of map compression
    regTot=3
    if np.random.uniform(0, 1.0, 1)[0]>0.8: regTot=6
    regTot+=hits*4 # tot per hit
    # other wise return 16 non-compressed
    return 16 #regTot


######################################
### plotting
######################################

# debug check agreement:
# print "direct bits:",totBits
# print "indirect bits:",sum([sum(overheadBits),sum(addBits),sum(dataBits)])

class DataSaver:

    def __init__(self, saveDir, fileName):
        self.saveName=saveDir
        if saveDir[-1]!="/":
            self.saveName+="/"
        self.saveName+=fileName

    def PieMaker(self, sumDict, title="NYS"):

        print "\n### DataSaver.PieMaker: title...",title
        print sumDict

        fig, ax = plt.subplots(figsize=(6, 3), subplot_kw=dict(aspect="equal"))


        wedges, texts, autotexts = ax.pie([i*100 for i in sumDict.values()], autopct='%1.1f%%', colors=['blue','grey','red','green'])

        ax.legend(wedges, sumDict.keys(),
          title="Ingredients",
          loc="center left",
          bbox_to_anchor=(1, 0, 0.5, 1))

        plt.setp(autotexts, size=8, weight="bold")

        ax.set_title(title)

        plt.subplots_adjust(top=0.95, bottom=0.05, left=0.05, right=0.95, hspace=0.40, wspace=0.409) # plots layout

        svNm=self.saveName
        if ".png" not in svNm:
            svNm+=".png"

        fig.savefig(svNm, bbox_inches='tight')


    def StatWriter(self, ds, pos, sumDict, argDict, title):

        print "\n### DataSaver.StatWriter: title...",title
        print sumDict

        # calculate datarate
        aveDatarate=float( sumDict['aveFramesPerEvent'] *(ds.afs) *ds.trig[argDict['layer']]*ds.over)/1000.0

        layerPos=title

        commandStr="_".join([str(k)+":"+str(v) for k,v in argDict.items()])

        statsStr="\n"
            #for s in [layerPos,sumDict.values()]:
            #statsStr+=str(s)+"\t"
        statsStr+=layerPos+"\t"+str(pos)+"\t"+str(argDict['layer'])+"\t"+argDict['region']+"\t"+"\t".join([str(v) for v in sumDict.values()])+"\t"+str(aveDatarate)+"\t"+commandStr+"\t"
        print statsStr

        svNm=self.saveName
        if ".txt" not in svNm:
            svNm+=".txt"

        statsFile=open(svNm,'w')

        statsFile.write("layerPos\tpos\tlayer\tregion\t"+"\t".join(sumDict.keys())+"\taveDatarate\tsimCommand\t")
        statsFile.write(statsStr)
        statsFile.close()

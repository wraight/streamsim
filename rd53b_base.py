
### code to simulate RD53B readout based on chip position: eta, layer, barrel/endcap

# includes streaming
'''
    The draft RD53B output format is based on the concept of data 'streams' that are concatenations of Aurora 64 bit frames... The readout chip is divided into 'cores' of 8x8 pixels (4x16) as the unit of data encoding for sensors with 50x50 (25x100) mum**2 pixels... Within each core, the bit map is constructed as a binary tree to identify all fired pixels... During the construction of the bit map there is a further compression applied for binary pairs with the tree, where a '01' is replaced by '0'. The RD53B data 'stream'.
    The RD53B data 'stream' is then assembled using the chip data from one to many events. The first bit of each 64 bit Aurora frame is reserved for the stream identification, it is 1 for the first frame of a stream, 0 for the following streams. For each event an 8 bit 'tag' data block is added containing the event header information for the first event in the 'stream', otherwise an extended 'tag' with '111' to signal a new 'tag' plus the 8 bit 'tag' information is used. Then the core column ('ccol') address is stored as 6 bits, followed by the core row address, that can be compressed and is stored as 5 or 6 bits. Next the 'bit map' for the pixel hits in the 'core' are added, followed by the 4 bit ToT information for each fired pixel. Then the information of the next core on the chip is added. If the core has the same ccol address, only the crow address is added. The stream is ended using a dynamic criteria that depends on the number of remaining empty bits that are left in the 64 bit Aurora frame after adding a core data block. If the number of empty bits is less than 'eStep' times the number of core data blocks already added to the stream, the remaining bits are left empty and a new data block is started with the 'tag' data and the next core data block. For this study, eStep= 1 is used that results in a compromised of the stream length. This parameter can be tuned in the future to adjust the average stream size for the different layers and to minimise the stream overheads. Finally, the same 4% overhead for the Aurora protocol for 'register frames' as for the RD53A protocol has to be taken into account.
    '''

import commonSimulationCode as csc
import ROOT
import numpy as np
import os
import time


######################################
### RD53B data encoding functions
######################################

def CheckNeighbours(coreArr):
    cArr=[]
    for c in range(0,len(coreArr),1):
        cx=coreArr[c]
        for d in range(c+1,len(coreArr),1):
            dx=coreArr[d]
            if dx==cx:
                if debug: print "coincident coloumn cores:",cx,",",dx
                cArr.append(d)
    return cArr

def CheckShare(neighComp, distNorm, count, test=False):
    doShare=False
    if neighComp>0 and int(np.random.choice(range(0,len(distNorm),1), size=1, p=distNorm))>1: # suppress neighbour address (0 ==> no neighbour suppression)
        doShare=True
        #print('ready to share column')
    elif test and count>0: doShare=True
    else:
        doShare=False
    return doShare

def DoShareCol(shareCol, ccol, dc, ds):
    if shareCol:
        if ds.debug: print "suppressed core column address"
        dc.colShare[-1]+=1
    else:
        dc.AddToCount({'b':len(ccol), 'tag':"cc_add"}, ds)
        dc.colShare.append(0)

def DoShareRow(shareRow, neighComp, dc, ds):
    qrow=10
    if shareRow:
        if ds.debug: print "suppressed core row address"
        qrow=2
        dc.rowShare[-1]+=1
    else:
        if neighComp==0: # edit (10/12/19) to remove neighbour suppression
            qrow=8
        dc.rowShare.append(0)
    return qrow

######################
### main
######################

def main():

    ######################################
    ### Commandline Arguments
    ######################################


    print csc.PrintSection("Commandline Arguments")
    argDict=csc.GetSimArgs() # to begin just get arguments
    if argDict['test']: # if test flag is set
        argDict=csc.SetTestArgs() # set test arguments
        argDict=csc.GetSimArgs(argDict) # adjust test arguments
    print "argDict:",argDict

    ### return
    ######################################
    ### Simulation Settings
    ######################################

    print csc.PrintSection("Simulation Settings")
    ds= csc.DataSettings()

    ### add user settings to data settings
    if argDict['debug']: ds.debug=True
    if argDict['eStep']>0: ds.eStep=int(argDict['eStep'])
    ds.trials=argDict['trials']


    ds.PrintSettings()

    # hits per region
    hitsAve=2.0
    hitsSigma=0.5


    ######################################
    ### Get Distributions
    ######################################

    print csc.PrintSection("Get Distributions")
    ### start with looping over z positions
    regDist=[] # regions per chip

    fileName=argDict['file'].replace("50x50",argDict['pitch'])
    '''
    corName="m_h2_cores_chip_"+argDict['region']+str(argDict['layer']) # hit regions per chip
    bitName="m_h2_bits_core_"+argDict['region']+str(argDict['layer'])
    if argDict['comp']==True:
        print "###\n###\ncompression on:",argDict['comp'],"###\n###\n"
        bitName="m_h2_bits_core_comp_"+argDict['region']+str(argDict['layer'])
    neiName="m_h2_rows_column_"+argDict['region']+str(argDict['layer'])
    '''
    '''
    branchName="RD53BUncompressedBitTree"
    if argDict['comp']==True:
        print "###\n###\ncompression on:",argDict['comp'],"###\n###\n"
        branchName="RD53BCompressedBitTree"
        '''
    branchName="encodingCompBTNS1"

    corName=branchName+"/m_h2_cores_per_chip_"+argDict['region']+str(argDict['layer']) # hit regions per chip
    bitName=branchName+"/m_h2_data_per_core_"+argDict['region']+str(argDict['layer'])
    colName=branchName+"/m_h2_qrows_per_ccol_"+argDict['region']+str(argDict['layer'])
    rowName=branchName+"/m_h2_neig_qrows_per_ccol_"+argDict['region']+str(argDict['layer'])
    if argDict['binary']==True:
        bitName=branchName+"/m_h2_bittree_per_core_"+argDict['region']+str(argDict['layer'])

    signal=ROOT.TFile(fileName)
    corHist=ROOT.TH2F()
    signal.GetObject(corName,corHist)
    bitHist=ROOT.TH2F()
    signal.GetObject(bitName,bitHist)
    colHist=ROOT.TH2F()
    signal.GetObject(colName,colHist)
    rowHist=ROOT.TH2F()
    signal.GetObject(rowName,rowHist)

    print "# N_binsX:",corHist.GetNbinsX()


    ######################################
    ### Loop over distribution bins
    ######################################

    print csc.PrintSection("Loop over distribution bins")

    for x in range(0,corHist.GetNbinsX(),1): # loop over z (x axis)

        if len(argDict['bins'])>0 and str(x) not in argDict['bins']: continue

        if ds.debug:
            print "check x:",x,"... string:",str(x)
            print "argDict bins(",len(argDict['bins']),"):",argDict['bins']

        bitDist=csc.GetDistArr(bitHist,x,-1) # bits per core
        corDist=csc.GetDistArr(corHist,x,-1) # cores per chip
        colDist=csc.GetDistArr(colHist,x,-1) # column occupancy
        rowDist=csc.GetDistArr(rowHist,x,-1) # next in row frequency
        zpos=corHist.GetXaxis().GetBinLowEdge(x)

        if ds.debug: print "z position:", zpos
        #continue

        # bail on empty positions
        if sum(corDist)==0: continue

        corDist=csc.ZeroBinCorrection(corDist,argDict['region'],argDict['layer'],zpos, 300)
        #print('corDist:\n'+str(corDist))
        corNorm=[ b/sum(corDist) for b in corDist]
        corAve= sum( [n*corNorm[n] for n in range(0,len(corNorm),1)]) /sum(corNorm)
        bitNorm=[ b/sum(bitDist) for b in bitDist]
        bitAve= sum( [n*bitNorm[n] for n in range(0,len(bitNorm),1)]) /sum(bitNorm)
        colNorm=[ b/sum(colDist) for b in colDist]
        colAve= sum( [n*colNorm[n] for n in range(0,len(colNorm),1)]) /sum(colNorm)
        rowNorm=[ b/sum(rowDist) for b in rowDist]
        rowAve= sum( [n*rowNorm[n] for n in range(0,len(rowNorm),1)]) /sum(rowNorm)
        '''
            print "### for z position",zpos,"..."
            print "# corDist("+str(sum(corDist))+"):"
            if argDict['debug']: print corDist
            print "# corNorm("+str(sum(corNorm))+"):"
            if argDict['debug']: print corNorm
            print "# ave corNorm:",corAve
            '''
        #print('colAve: '+str(colAve))
        #print('rowAve: '+str(rowAve))
        #print('row\n'+str(rowNorm))
        #continue

        ####################################
        ### Run Simulation
        ####################################

        print csc.PrintSection("Run Simulation")

        # DataCounter
        dc=csc.DataCounter(ds, "rd53b")

        NhitPix=0
        hitCores=[]

        ### if multCut on then switch eStep off!
        if argDict['multCut']>0:
            if ds.debug: print "### MULTI-CUT MODE ACTIVATED\n... multCut",argDict['multCut']
            ds.eStep=-1
            ds.multCut=int(argDict['multCut'])


        # generating array of number of hit regions per event
        for e in range(0,argDict['trials'],1):
            Ncores=int(np.random.choice(range(0,len(corNorm),1), size=1, p=corNorm))
            if argDict['test']: Ncores=5

             # empty chips suppression
            if argDict['empties']==0:
                while Ncores==0: # empty chip suppression
                    Ncores=int(np.random.choice(range(0,len(corNorm),1), size=1, p=corNorm))

            if Ncores==0: dc.empties+=1
            dc.regions.append(Ncores)


        # manually set hit regions in event for debug
        #hitCores=[6]
        beginSim=True


        if ds.debug: print "\n### events and hit cores..."
        if ds.debug: print "### hitCores("+str(len(hitCores))+"):",hitCores

        checkStreams=[]
        checkFrames=[]
        checkEVal=[]

        # loop events
        for hc in dc.regions:

            # new event tag (inc. empty events)
            ### todo: check streaming option

            # skip stuff for first event
            if beginSim==True:
                #dc.AddToCount(ds.tagY,ds)
                beginSim=False
            else:
                if ds.multCut>0 and dc.events%ds.multCut==0:
                    if ds.debug: print "ending stream"
                    dc.MultChop(ds)
                    dc.eVal=0

            # new event tag
            if (dc.eVal)**2>0:
                dc.AddToCount(ds.tagY,ds)
                dc.CheckEval(ds) # compare remaining to eStep after data
            dc.events+=1
            count=0


            #print csc.PrintSection("Pre-loop")
            #dc.PrintData()

            # looping over hit cores
            for c in range(0,hc,1):
                # generating randomly uniform arrays of hit region coordinates
                hcX= int(np.random.uniform(0, 400/8, 1)[0])
                hcY= int(np.random.uniform(0, 384/8, 1)[0])

                if ds.debug: print "core:",hcX,",",hcY


                # core column (X coordinate) is uncompressed: range (1-55)
                ccol=format(hcX+1,'06b') # bit-ified core column address
                doShareCol=CheckShare(argDict['neighComp'], colNorm, count, argDict['test'])
                DoShareCol(doShareCol, ccol, dc, ds)

                # Maurice: 8 bit quarter core row address (suppressed id neighboring) + 2 bits isneighbor and islast
                qrow=10 # bit-ified core row address
                doNextRow=CheckShare(argDict['neighComp'], rowNorm, count, argDict['test'])
                qrow=DoShareRow(doNextRow, argDict['neighComp'], dc, ds)
                count+=1

                dc.AddToCount({'b':qrow, 'tag':"cr_add"}, ds)
                dc.eVal+=ds.eStep # increment eStep after addressing

                if ds.debug: print "ccol("+str(len(ccol))+"):",ccol
                if ds.debug: print "crow("+str(qrow)+"):",{'b':qrow, 'tag':"cr_add"}

                # hit pixels in this core
                coreBits= int(np.random.choice(range(0,len(bitNorm),1), size=1, p=bitNorm))
                if argDict['test']: coreBits= 10
                if ds.debug: print "coreBits:",coreBits
                checkEVal.append(coreBits)

                ### now have all the data encoded for core so can add to stream
                dc.AddToCount({'b':coreBits, 'tag':"core_tot"}, ds)

                dc.CheckEval(ds) # compare remaining to eStep after data

                '''
                    #region occupancy across z as test plot
                    #data based on 32 bit region
                    #0.5 neighbour prob (check this, make tunable)
                '''

                #close the core loop (each event)

            # extra noise-like hits: add each hit as separate region (fixed treelength + ToT)
            extraHits=int(round(hc*(argDict['extraNoise']/100.0)))
            dc.Xregions.append(extraHits)
            for i in range (0,extraHits,1):
                if argDict['debug']: print "Adding",i,"of",argDict['extraHits'],"extra hits"
                dc.AddToCount({'b':6, 'tag':"cc_add"}, ds)
                dc.AddToCount({'b':10, 'tag':"cr_add"}, ds)
                dc.eVal+=ds.eStep # increment eStep after addressing
                #coreBits= int(np.random.choice(range(0,len(bitNorm),1), size=1, p=bitNorm))
                coreBits=10
                checkEVal.append(coreBits)
                dc.AddToCount({'b':coreBits, 'tag':"core_tot"}, ds)
                dc.CheckEval(ds) # compare remaining to eStep after data


            # extra track-like hits: add each hit as separate region (full address + ToT)
            extraHits=int(round(hc*(argDict['extraHits']/100.0)))
            dc.Xregions.append(extraHits)
            for i in range (0,extraHits,1):
                if argDict['debug']: print "Adding",i,"of",argDict['extraHits'],"extra hits"

                # core column (X coordinate) is uncompressed: range (1-55)
                ccol=format(hcX+1,'06b') # bit-ified core column address
                doShareCol=CheckShare(argDict['neighComp'], colNorm, i, argDict['test'])
                DoShareCol(doShareCol, ccol, dc, ds)

                # Maurice: 8 bit quarter core row address (suppressed id neighboring) + 2 bits isneighbor and islast
                doNextRow=CheckShare(argDict['neighComp'], rowNorm, i, argDict['test'])
                qrow=DoShareRow(doNextRow, argDict['neighComp'], dc, ds)
                dc.AddToCount({'b':qrow, 'tag':"cr_add"}, ds)
                dc.eVal+=ds.eStep # increment eStep after addressing

                # hit pixels in this core
                coreBits= int(np.random.choice(range(0,len(bitNorm),1), size=1, p=bitNorm))
                checkEVal.append(coreBits)
                dc.AddToCount({'b':coreBits, 'tag':"core_tot"}, ds)
                dc.CheckEval(ds) # compare remaining to eStep after data

            dc.fpe.append(sum([len(fa) for fa in dc.streams])+len(dc.frameArr)-sum(dc.fpe))
            checkStreams.append(len(dc.streams))
            checkFrames.append(len(dc.frameArr))


        #close the events loop

        print csc.PrintSection("Post-loop")

        dc.TidyUp(ds)

        if argDict['debug']:
            dc.PrintData()
            csc.PrintColourStream(dc.streams, 'b')
            dc.EndCheck()
            ds.PrintSettings()

        print csc.PrintSection("Summary: rd53b "+argDict['pitch']+" "+argDict['region']+" l:"+str(argDict['layer'])+" z:"+str(zpos))

        dc.PrintSummary(ds)

        saveName="rd53b_"+argDict['pitch']+"_"+argDict['region']+"_l"+str(argDict['layer'])+"_z"+str(zpos)+"_es"+str(argDict['eStep'])
        dSave= csc.DataSaver(os.getcwd()+"/"+argDict['outDir'],saveName)

        dSave.StatWriter(ds, zpos, dc.sumDict, argDict, saveName)

        #dc.PrintDict()
        #if argDict['debug']:
        if ds.debug==1:
            dc.PrintDict("RegPerEvent")
            dc.PrintDict("FramesPerEvent")
            dc.PrintDict("BitsPerEvent")
        '''
        print "frames per event ("+str(len(dc.fpe))+"):",dc.fpe
        print "streams size as event ("+str(len(checkStreams))+"):",checkStreams
        print "frameArr size as event ("+str(len(checkFrames))+"):",checkFrames
        print "eVal size as event ("+str(len(checkEVal))+"):",checkEVal
        '''

    #dc.PrintSummary(ds)
    #csc.PrintColourStream(dc.streams)
    #csc.PrintColourStream(dc.streams, "tag")





if __name__ == "__main__":
    print "### in",__file__,"###"
    start = time.time()
    main()
    end = time.time()
    print "\n+++ Total scan time: ",(end-start),"seconds +++\n"
    print "### out",__file__,"###"


exit()
